A publicly accessible version of the foemorelesszen theme for deployment by more-onion

`/foemorelesszen` within this repository should contain a complete copy of `/drupal/sites/all/themes/foemorelesszen` from the `cms-campaignion` private repository, including all production-built CSS/JS files.

# "Deployment" steps

## 1. Within the `cms-campaignion` repo

1. Ensure `cms-campaignion` is up to date with the `master` branch:
```
git checkout master && git pull
```

2. Ensure the `cms-campaignion` development container ("`drupal`") is running:
```
./start.sh
```

3. Open a `bash` shell inside the `drupal` container, running as the `drupal` user:
```
cd amazee-docker
docker-compose exec -u drupal drupal bash
```

4. Navigate to the `foemorelesszen` theme directory and generate a production build:
```
cd /var/www/drupal/public_html/sites/all/themes/foemorelesszen
yarn install
yarn run build
```
5. Exit the `bash` shell:
```
exit
```



## 2. Within the `foemorelesszen` repo

1. Ensure `foemorelesszen` is up to date with the `master` branch:
```
git checkout master && git pull
```

2. Remove the entire existing `foemorelesszen` subdirectory:
```
rm -rf foemorelesszen
```

3. Copy the `foemorelesszen` subdirectory over from `cms-campaignion`:

    (This assumes the repository directories are side-by-side on your computer. Adjust as needed.)
```
cp -r ../cms-campaignion/drupal/sites/all/themes/foemorelesszen ./
```

4. Check that the change lists look as expected:
```
git status
# and/or
git diff
```

5. Add all changes and create a commit (directly on the `master` branch):
```
git add .
git commit -m "Capture changes from cms-campaigion 20YY-MM-DD - Enable feature xxyyzz"
git push
```

## 3. On the BitBucket website

1. Check that your commit is visible at the top of https://bitbucket.org/foeewni/foemorelesszen/commits/

2. Take note of the previous release tag (`vA.B.C`)

3. Go into your commit and add a semantic version tag as appropriate -- usually just a patch bump (`C`), sometimes a point release bump (`B`), almost never a full release bump (`A`).

4. Grab the full URL of the new tag, e.g. https://bitbucket.org/foeewni/foemorelesszen/commits/tag/vA.B.C

## 4. Via email

1. Tell More Onion support that there's a new theme version, and provide the new tag's URL as copied above.

2. Wait for them to test the code and deploy to their staging.

3. Test on their staging: https://foeuk.drupal7.more-onion.at/

4. Confirm testing success with them, and coordinate a production release timeslot.