const presets = [
  [
    '@babel/preset-env',
    {
      targets: {
        ie: '11',
        firefox: '30',
        chrome: '40',
        safari: '7',
      },
      useBuiltIns: 'usage',
      corejs: '2',
      modules: 'commonjs',
    },
  ],
];
const plugins = [];

module.exports = {
  presets,
  plugins,
};
