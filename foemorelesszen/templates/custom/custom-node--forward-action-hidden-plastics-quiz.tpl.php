<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: whether submission information should be displayed.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>

<?php $classes .= ' node-petition-sidebar'; ?>


<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>


  <!-- BEGIN Forward Action CSS -->
    <link href="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/css/main.css" rel="stylesheet">
    <!--
      ...it appears to be okay to have a <link> in the <body>, but assume it'll sometimes cause a FOUC, so perhaps this CSS
      should be inlined. See https://html.spec.whatwg.org/multipage/links.html#link-type-stylesheet:link-type-stylesheet
    -->
  <!-- END Forward Action CSS -->


  <!-- BEGIN Forward Action HTML -->
  <div id="slide-container">

    <!-- Ini slide and question 0 -->
    <div class="slide" id="slide0" data-slide="0">
      <div class="container">
        <div class="row slide0-top mb50">
          <div class="col-md-6">
            <div class="text-title-deco">Can you spot the hidden plastics?</div>
          </div>
          <div class="col-md-6">
            <div class="text-default">Right now, millions of us are trying to cut down how much plastic we use. But you’d be amazed at some of the everyday things that contain hidden plastic, with hardly any of us realising it.</div>
            <div class="text-default-bigger">Can you guess which of these items secretly contain plastic? Very few people get them all right…</div>
          </div>
        </div>
        <div class="row questions-container">
          <div class="col-md-12">
            <div class="text-question">So, which of these often contains hidden plastic?</div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="0" data-a="0">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/0a.jpg" class="question-card-img" />
              <div class="question-card-text">Chewing gum</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="0" data-a="1">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/0b.jpg" class="question-card-img" />
              <div class="question-card-text">Unpackaged processed cheese</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="0" data-a="2">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/0c.jpg" class="question-card-img" />
              <div class="question-card-text">Loose rice</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <div class="col-md-12 progress-container">
            <div class="progress-bar-container">
              <div class="progress-bar-inner q1"></div>
            </div>
            <div class="text-progress">Question 1 of 8</div>
          </div>
        </div>
      </div>
    </div>

    <!-- Question 1 -->
    <div class="slide" id="slide1" data-slide="1">
      <div class="container">
        <div class="row questions-container">
          <div class="col-md-12">
            <div class="text-question">Which of these everyday items usually comes with some sneaky plastic?</div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="1" data-a="0">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/1a.jpg" class="question-card-img" />
              <div class="question-card-text">Paper clips</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="1" data-a="1">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/1b.jpg" class="question-card-img" />
              <div class="question-card-text">Newspapers</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="1" data-a="2">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/1c.jpg" class="question-card-img" />
              <div class="question-card-text">Teabags</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <div class="col-md-12 order-first order-md-12 progress-container">
            <div class="progress-bar-container">
              <div class="progress-bar-inner q2"></div>
            </div>
            <div class="text-progress">Question 2 of 8</div>
          </div>
        </div>
      </div>
    </div>

    <!-- Question 2 -->
    <div class="slide" id="slide2" data-slide="2">
      <div class="container">
        <div class="row questions-container">
          <div class="col-md-12">
            <div class="text-question">Where might you find some hidden plastic in the kitchen?</div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="2" data-a="0">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/2a.jpg" class="question-card-img" />
              <div class="question-card-text">Foil</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="2" data-a="1">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/2b.jpg" class="question-card-img" />
              <div class="question-card-text">Egg boxes</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="2" data-a="2">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/2c.jpg" class="question-card-img" />
              <div class="question-card-text">Cartons</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <div class="col-md-12 order-first order-md-12 progress-container">
            <div class="progress-bar-container">
              <div class="progress-bar-inner q3"></div>
            </div>
            <div class="text-progress">Question 3 of 8</div>
          </div>
        </div>
      </div>
    </div>

    <!-- Question 3 -->
    <div class="slide" id="slide3" data-slide="3">
      <div class="container">
        <div class="row questions-container">
          <div class="col-md-12">
            <div class="text-question">Which of these drinking containers has been <em>ruined</em> by pesky plastic?</div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="3" data-a="0">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/3a.jpg" class="question-card-img" />
              <div class="question-card-text">Glass bottles</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="3" data-a="1">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/3b.jpg" class="question-card-img" />
              <div class="question-card-text">Enamel mugs</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="3" data-a="2">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/3c.jpg" class="question-card-img" />
              <div class="question-card-text">Paper cups (without lids)</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <div class="col-md-12 order-first order-md-12 progress-container">
            <div class="progress-bar-container">
              <div class="progress-bar-inner q4"></div>
            </div>
            <div class="text-progress">Question 4 of 8</div>
          </div>
        </div>
      </div>
    </div>

    <!-- Question 4 -->
    <div class="slide" id="slide4" data-slide="4">
      <div class="container">
        <div class="row questions-container">
          <div class="col-md-12">
            <div class="text-question">What about these familiar items?&nbsp;&nbsp;&nbsp;¯\_(ツ)_/¯</div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="4" data-a="0">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/4a.jpg" class="question-card-img" />
              <div class="question-card-text">Bricks</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="4" data-a="1">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/4b.jpg" class="question-card-img" />
              <div class="question-card-text">Crisp packets</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="4" data-a="2">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/4c.jpg" class="question-card-img" />
              <div class="question-card-text">Glass jars</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <div class="col-md-12 order-first order-md-12 progress-container">
            <div class="progress-bar-container">
              <div class="progress-bar-inner q5"></div>
            </div>
            <div class="text-progress">Question 5 of 8</div>
          </div>
        </div>
      </div>
    </div>

    <!-- Question 5 -->
    <div class="slide" id="slide5" data-slide="5">
      <div class="container">
        <div class="row questions-container">
          <div class="col-md-12">
            <div class="text-question">Guess what? One of these contains plastic too!</div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="5" data-a="0">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/5a.jpg" class="question-card-img" />
              <div class="question-card-text">Light bulbs</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="5" data-a="1">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/5b.jpg" class="question-card-img" />
              <div class="question-card-text">Matches</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="5" data-a="2">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/5c.jpg" class="question-card-img" />
              <div class="question-card-text">Tote bags</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <div class="col-md-12 order-first order-md-12 progress-container">
            <div class="progress-bar-container">
              <div class="progress-bar-inner q6"></div>
            </div>
            <div class="text-progress">Question 6 of 8</div>
          </div>
        </div>
      </div>
    </div>

    <!-- Question 6 -->
    <div class="slide" id="slide6" data-slide="6">
      <div class="container">
        <div class="row questions-container">
          <div class="col-md-12">
            <div class="text-question">Almost done, you know what to do. Plastic is hidden everywhere!</div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="6" data-a="0">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/6a.jpg" class="question-card-img" />
              <div class="question-card-text">Porcelain plates</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="6" data-a="1">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/6b.jpg" class="question-card-img" />
              <div class="question-card-text">Wet wipes</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="6" data-a="2">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/6c.jpg" class="question-card-img" />
              <div class="question-card-text">Coloured soaps</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <div class="col-md-12 order-first order-md-12 progress-container">
            <div class="progress-bar-container">
              <div class="progress-bar-inner q7"></div>
            </div>
            <div class="text-progress">Question 7 of 8</div>
          </div>
        </div>
      </div>
    </div>

    <!-- Question 7 -->
    <div class="slide" id="slide7" data-slide="7">
      <div class="container">
        <div class="row questions-container">
          <div class="col-md-12">
            <div class="text-question">Last one. Who brings plastic for lunch?</div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="7" data-a="0">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/7a.jpg" class="question-card-img" />
              <div class="question-card-text">Wooden cutlery</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="7" data-a="1">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/7b.jpg" class="question-card-img" />
              <div class="question-card-text">Cardboard food boxes</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <!-- Answer column -->
          <div class="col-md-4 question-card-container">
            <div class="question-card question-card-option skip" data-q="7" data-a="2">
              <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/q/7c.jpg" class="question-card-img" />
              <div class="question-card-text">Paper bags</div>
              <div class="question-card-icon">
                <img src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/img/icon-check.png" class="question-card-icon-img" />
              </div>
            </div>
          </div>
          <div class="col-md-12 order-first order-md-12 progress-container">
            <div class="progress-bar-container">
              <div class="progress-bar-inner q8"></div>
            </div>
            <div class="text-progress">Question 8 of 8</div>
          </div>
        </div>
      </div>
    </div>

    <!-- Before results -->
    <div class="slide slide-petition-variants" id="slide-before-results" data-slide="8">
      <div class="container before-results-container">
        <div class="row">
        <div class="col-sm-6 order-sm-6">
            <div class="text-title mobile-only text-center mb30 slide-petition-variants-title-mobile">While we check your answers ...</div>
            <div class="card card-blue">
              <div class="text-cta mb50 slide-petition-variants-cta">Would you sign a petition telling the government to act now to reduce the plastic pouring into our oceans?</div>
              <div class="buttons-container buttons-container-50">
                <button class="btn btn-primary skip" data-target="slide-form">Yes, of course</button>
                <button class="btn btn-primary btn-part2" style="opacity:.8;">No, sorry</button>
              </div>
            </div>
          </div>
          <div class="col-sm-6 order-sm-1 before-result-text">
            <div class="text-title text-white mb20 desktop-only slide-petition-variants-title">While we check your answers, do you have a few seconds to help save dolphins, turtles and other animals under threat from plastic pollution?</div>
            <div class="text-default text-white mb20 slide-petition-variants-text1">Every year we throw away millions of tonnes of plastic – sometimes without even knowing – and a vast amount finds its way into the ocean. As the plastic breaks down into tiny pieces it’s consumed by marine animals, killing hundreds of thousands a year.</div>
            <div class="text-default text-white slide-petition-variants-text2">We can’t stop this from happening unless the government takes responsibility.</div>
          </div>
        </div>
      </div>
    </div>


    <!-- Form slide -->
    <div class="slide" id="slide-form" data-slide="9">
      <div class="container form-slide-container">
        <div class="row">

          <div class="col-12 col-sm-10 offset-sm-1 col-md-4 offset-md-0">
            <div class="field-body petition-copy">
              <?php print render($content['body']); ?>
            </div>
          </div>

          <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-0">
            <div class="petition-sidebar">
              <?php print render($content['webform']); ?>
            </div>
          </div>

        </div>
      </div>
    </div>


  </div><!-- END slide-container -->
  <!-- END Forward Action HTML -->



  <!-- BEGIN Forward Action Javascript -->
  <script type="text/javascript" src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/js/abtester.min.js"></script>
  <script type="text/javascript" src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/js/tests.js"></script>
  <script type="text/javascript" src="/<?php print path_to_theme(); ?>/assets/hidden-plastics-quiz/static/js/main.js"></script>
  <!-- END Forward Action Javascript -->



</article>