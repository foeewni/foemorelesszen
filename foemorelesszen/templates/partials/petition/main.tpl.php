<?php $classes .= ' node-petition-sidebar'; ?>
<article data-nid="<?php print $node->nid; ?>" id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php
    $template = drupal_get_path('theme', 'foemorelesszen').'/templates/partials/petition/'._foemorelesszen_get_layout().'.tpl.php';
    if (file_exists($template)) {
      include($template);
    } else {
      include(drupal_get_path('theme', 'foemorelesszen').'/templates/partials/petition/default.tpl.php');
    }
  ?>
</article>
