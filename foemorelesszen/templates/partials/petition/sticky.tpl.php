<section class="foe-new-ux-main-2018">
  <div class="new-ux-sticky-container">
    <?php
      // Uncomment the next chunk of code if you want to get a specific style image from the URL instead of letting drupal render the block.
      // Keep in mind the renderer is a media entity, so sometimes prints images, sometime prints videos... Check against [0]['type'].

      $field_main_image = field_get_items('node', $node, 'field_main_image');
      $field_hero_image = field_get_items('node', $node, 'field_hero_image');
      $image_uri = FALSE;
      $hero_inline_style = '';

      if ($field_hero_image && $field_hero_image[0]['type'] != 'video') {
        $image_uri = $field_hero_image[0]['uri'];
      } else if ($field_main_image && $field_main_image[0]['type'] != 'video') {
        $image_uri = $field_main_image[0]['uri'];
      }

      if ($image_uri) {
        $style_name = 'background_image';
        $derivative_uri = image_style_path($style_name, $image_uri);
        $success = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style_name), $image_uri, $derivative_uri);
        $hero_inline_style = 'background-image:url(' . file_create_url($derivative_uri) . ');';
      }
    ?>
    <?php
      /**
       * @todo delete this if we push this live after optimize tests
       */
      $node_alias = array_reverse(explode('/', drupal_get_path_alias()))[0];
    ?>
    <header class="article-header <?php echo $node_alias; ?>" style="<?php echo $hero_inline_style; ?>">
      <div class="col-12 col-sm-10 offset-sm-1">
        <?php print render($title_prefix); ?>
        <h1>
          <span class="display-1" <?php print $title_attributes; ?>>
          <?php if ( field_get_items('node', $node, 'field_public_title') ): 
            print render($content['field_public_title']); 
          else: 
            print $title; 
          endif ?>
          </span>          
          
          <?php if ( field_get_items('node', $node, 'field_subtitle') ): ?>
            <span class="display-2" <?php print $title_attributes; ?>><?php print render($content['field_subtitle']); ?></span>
          <?php endif ?>
        </h1>
        <?php print render($title_suffix); ?>
      </div>
    </header>

    <?php $has_counter = field_get_items('node', $node, 'pgbar_default'); ?>
    <?php if ($has_counter): ?>
      <div class="progress-bar-mobile d-block d-md-none">
        <?php print render($content['pgbar_default']); ?>
      </div>
    <?php endif ?>

    <section class="article-body"<?php print $content_attributes; ?>>
      <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2 col-xxl-6 offset-xxl-3">
        <div class="field-body petition-copy">
          <?php
            // We hide the comments, tags and links now so that we can render them later.
            hide($content['field_main_image']);
            hide($content['field_hero_image']);
            hide($content['comments']);
            hide($content['links']);
            hide($content['field_tags']);
            hide($content['field_campaign_type']);
            hide($content['recent_supporters']);
            hide($content['pgbar_default']);
            hide($content['webform']);
            print render($content);
          ?>
        </div>
      </div>
    </section>

  </div>
  <?php $supporters_active = field_get_items('node', $node, 'recent_supporters')[0]['toggle']; ?>
  <?php if ($supporters_active == 1): ?>
    <div class="new-ux-supporters d-none d-md-block">
      <?php
        print render($content['recent_supporters']);
      ?>
    </div>
  <?php endif ?>

  <aside class="new-ux-sticky">
    <div class="petition-sidebar">
      <div class="prefill-salutation element-invisible">
        <h3><span class="js-prefill-name"></span>, sign the petition</h3>
        <a href="#" class="js-prefill-reset">not you?</a>
      </div>
      <div class="d-none d-md-block">
        <?php print render($content['pgbar_default']); ?>
      </div>

      <?php
        print render($content['webform']);
      ?>
    </div>
    <?php
      global $base_url;
      $node_absolute_url = $base_url . $node_url;
    ?>
  </aside>

  <?php $supporters_active = field_get_items('node', $node, 'recent_supporters')[0]['toggle']; ?>
  <?php if ($supporters_active == 1): ?>
    <div class="new-ux-supporters d-block d-md-none">
      <?php
        print render($content['recent_supporters']);
      ?>
    </div>
  <?php endif ?>

  <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
    <footer>
      <?php print render($content['field_tags']); ?>
      <?php print render($content['links']); ?>
    </footer>
  <?php endif; ?>


  <?php // This thingie (text and visibility) is driven from JS ?>
  <div class="new-ux-fixed-button text-center">
    <button class="d-inline-block" data-scroll-to=".new-ux-sticky"></button>
  </div>
</section>
