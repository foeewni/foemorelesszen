<section class="foe-petition-main-2017">
  <div class="container">
    <header class="row article-header">
      <div class="col-12">

        <?php print render($title_prefix); ?>
        <h1>
          <span class="display-1" <?php print $title_attributes; ?>>
          <?php if ( field_get_items('node', $node, 'field_public_title') ): 
            print render($content['field_public_title']); 
          else: 
            print $title; 
          endif ?>
          </span>
          <?php if ( field_get_items('node', $node, 'field_subtitle') ): ?>
            <span class="display-2" <?php print $title_attributes; ?>><?php print render($content['field_subtitle']); ?></span>
          <?php endif ?>
        </h1>
        <?php print render($title_suffix); ?>

      </div>
    </header>
    <?php
      $field_main_image = field_get_items('node', $node, 'field_main_image');

      // Uncomment the next chunk of code if you want to get a specific style image from the URL instead of letting drupal render the block.
      // Keep in mind the renderer is a media entity, so sometimes prints images, sometime prints videos... Check against [0]['type'].

      /*

      if ($field_main_image && $field_main_image[0]['uri'] != 'video') {
        $style_name = 'background_image';
        $image_uri = $field_main_image[0]['uri'];
        $derivative_uri = image_style_path($style_name, $image_uri);
        $success = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style), $image_uri, $derivative_uri);
        $main_image_url  = file_create_url($derivative_uri);
      }
      */
    ?>

    <section class="article-body row"<?php print $content_attributes; ?>>
      <div class="col-12 col-md-6 col-lg-7 col-xl-7">
        <div class="field-body petition-copy">
          <?php
            // We hide the comments, tags and links now so that we can render them later.
            hide($content['field_main_image']);
            hide($content['field_hero_image']);
            hide($content['comments']);
            hide($content['links']);
            hide($content['field_tags']);
            hide($content['field_campaign_type']);
            hide($content['recent_supporters']);
            hide($content['pgbar_default']);
            hide($content['webform']);
          ?>
          <?php if ($field_main_image) : ?>
            <div class="petition-main-image mb-4 pb-4">
              <?php print render($content['field_main_image']); ?>
            </div>
          <?php endif; ?>
          <?php print render($content); ?>
        </div>
      </div>
      <aside class="col-12 col-md-6 col-lg-5 col-xl-5">
        <div class="petition-sidebar">
          <div class="prefill-salutation element-invisible">
            <h3><span class="js-prefill-name"></span>, sign the petition</h3>
            <a href="#" class="js-prefill-reset">not you?</a>
          </div>
          <?php
            print render($content['pgbar_default']);
            print render($content['webform']);
          ?>
        </div>
        <?php
          global $base_url;
          $node_absolute_url = $base_url . $node_url;
        ?>
        <div class="petition-supporters pt-4">
          <?php $supporters_active = field_get_items('node', $node, 'recent_supporters')[0]['toggle']; ?>
          <?php if ($supporters_active == 1): ?>
            <h3><?= t('Our supporters'); ?></h3>
            <?php
              print render($content['recent_supporters']);
            ?>
          <?php endif ?>
        </div>
      </aside>
    </div>
  </div>

  <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
    <footer>
      <?php print render($content['field_tags']); ?>
      <?php print render($content['links']); ?>
    </footer>
  <?php endif; ?>

  <?php print render($content['comments']); ?>
</section>
