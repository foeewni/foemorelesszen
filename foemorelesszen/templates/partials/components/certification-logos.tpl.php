<div class="logos">
  <div class="logos-left d-flex flex-column flex-md-row justify-content-start">
    <div class="logo logo-regulator dma">
      <img class="img-fluid-h" src="/<?php print path_to_theme(); ?>/img/dma.png"  alt="DMA Logo"/>
    </div>
    <div class="logo logo-regulator fundraising-regulator">
      <img class="img-fluid-h" src="/<?php print path_to_theme(); ?>/img/fundraising-regulator.png"  alt="Fundraising Regulator Logo"/>
    </div>
  </div>
  
  <div class="logos-right d-flex flex-column flex-md-row justify-content-end">
    <div class="logo logo-payment visa">
      <img class="img-fluid-h" src="/<?php print path_to_theme(); ?>/img/payment/visa-white.svg"  alt="Visa Logo"/>
    </div>
    <div class="logo logo-payment mastercard">
      <img class="img-fluid-h" src="/<?php print path_to_theme(); ?>/img/payment/mastercard-white.svg"  alt="Mastercard Logo"/>
    </div>
    <div class="logo logo-payment paypal">
      <img class="img-fluid-h" src="/<?php print path_to_theme(); ?>/img/payment/paypal-white.svg"  alt="Paypal Logo"/>
    </div>
    <?php if (field_get_items('node', $node, 'field_mobile_payment_logos')[0]['value'] === '1'): ?>
    <div class="logo logo-payment google-pay">
      <img class="img-fluid-h" src="/<?php print path_to_theme(); ?>/img/payment/google-pay.svg" alt="Google Pay Logo" />
    </div>
    <div class="logo logo-payment apple-pay">
      <img class="img-fluid-h" src="/<?php print path_to_theme(); ?>/img/payment/apple-pay.svg" alt="Apple Pay Logo" />
    </div>
    <?php endif; ?>
  </div>
</div>