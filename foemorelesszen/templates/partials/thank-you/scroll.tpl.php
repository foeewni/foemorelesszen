<?php
/*
 * @file partials/thank-you/scroll.tpl.php
 * @project foemorelesszen
 * @description This file contains the scrolling partial for the thank you page, invoked from the node--thank-you-page.tpl
 *
 * @author Luigi Mannoni (hello@luigimannoni.com)
 * @created Wednesday, 4th July 2018 12:25:13 pm
 *
 * @last-modified Wednesday, 4th July 2018 12:25:17 pm
 *                Luigi Mannoni (hello@luigimannoni.com)
 */

  $field_main_image = field_get_items('node', $node, 'field_main_image');

  if ($field_main_image && $field_main_image[0]['uri'] != 'video') {
    $style_name = 'background_image';
    $image_uri = $field_main_image[0]['uri'];
    $derivative_uri = image_style_path($style_name, $image_uri);
    $success = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style_name), $image_uri, $derivative_uri);
    $main_image_url = file_create_url($derivative_uri);
  } else {
    $main_image_url = 'data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" viewBox="-100 -100 708 485.75" height="1080" width="1920"><g fill="maroon" fill-opacity=".5"><path d="M92.237 204.182l10.349-2.793-.543-2.01-3.048.401-1.135-.693-6.617-24.523 26.204 22.815 3.237-.874-7.995-29.623.645-1.121 2.824-1.236-.53-1.962-10.3 2.78.53 1.962 3.062-.353 1.12.645 6.222 23.05L92.76 170.05l-.331-1.226-9.22 2.488.528 1.962 3.219-.553 1.068.449 7.187 26.631-.631 1.17-2.886 1.2zM134.624 156.543c-4.218 1.138-7.5 3.602-9.675 7.24-2.425 4.076-2.95 8.953-1.508 14.299 1.442 5.346 4.364 9.346 8.572 11.683 3.586 1.979 7.76 2.43 11.928 1.306 5.69-1.536 9.827-5.704 11.303-11.153.9-3.295.86-6.757-.12-10.386-1.442-5.346-4.4-9.284-8.546-11.585-3.71-2.05-7.737-2.543-11.954-1.404zm.714 2.648c6.327-1.707 12.175 2.608 14.636 11.73 2.462 9.123-.407 15.843-6.734 17.55-6.376 1.721-12.237-2.643-14.698-11.766-2.489-9.22.42-15.793 6.796-17.514zM176.924 181.328l12.409-3.349-.543-2.01-3.205.601-1.02-.461-7.292-27.024.649-.912 3.06-1.141-.53-1.962-12.409 3.348.53 1.962 3.218-.552 1.019.461 7.293 27.024-.649.912-3.072 1.092zM192.63 177.09l10.348-2.793-.543-2.01-3.097.414-1.134-.693-7.227-26.78 19.309 27.097 2.109-.57 3.365-33.74 7.465 27.661-.306.82-3.17 1.118.542 2.01 11.722-3.162-.543-2.011-3.156.588-.773-.527-7.293-27.025.403-.845 3.01-1.128-.529-1.962-10.496 2.832.41 1.52-2.727 26.94-15.865-22.132-.357-1.324-10.398 2.805.53 1.962 3.316-.579.823.515 7.187 26.631-.631 1.17-2.837 1.187zM238.487 128.987l-3.191 33.8-2.686 1.356.543 2.011 9.613-2.594-.543-2.011-3.254.615-.676-.554.954-10.308 11.575-3.123 6.06 8.415-.159.779-2.974 1.066.542 2.01 11.134-3.004-.543-2.01-3.102.205-20.056-27.526zm2.174 4.938l8.718 12.223-9.907 2.673zM281.06 136.283l.542 2.01 3.548-.694 1.02.462 2.752 10.202c-1.793.957-3.408 1.603-4.83 1.987-7.7 2.078-14.1-2.14-16.602-11.41-2.383-8.828.91-15.347 8.413-17.372 1.962-.53 3.896-.577 5.936-.233l.925.697 1.94 4.265 2.746-.741-2.104-7.799c-3.489-.058-6.809.312-9.8 1.12-10.84 2.924-15.808 11.421-13.015 21.77 1.734 6.425 6.457 11.253 12.561 12.816 2.6.666 5.68.572 8.965-.315 2.894-.78 6.12-2.283 9.69-4.456l-3.164-11.722.649-.912 2.631-.973-.543-2.01zM300.494 147.981l25.258-6.816-2.157-7.995-2.55.689.502 5.178-.475.97-13.144 3.547-3.706-13.732 8.044-2.17.885.55 1.443 3.398 2.305-.622-2.911-10.79-2.306.622.476 3.713-.501.872-8.044 2.17-3.348-12.408 11.33-3.057.884.55 1.812 3.984 2.747-.742-1.933-7.16-23.492 6.34.529 1.961 2.875-.46 1.019.462 7.293 27.024-.649.912-2.73 1zM343.414 135.346c2.521.372 5.72.14 9.252-.813 7.847-2.118 11.604-7.499 10.042-13.286-.582-2.158-1.807-3.774-3.614-4.813-2.851-1.598-5.711-1.668-8.49-2.023-5.03-.695-7.714-1.286-8.601-4.572-.966-3.58.9-6.61 5.069-7.734 1.716-.464 3.28-.517 4.692-.162l.96.636 1.993 4.46 2.55-.688-2.05-7.602c-3.146-.15-6.011.149-8.709.877-6.964 1.88-10.284 6.932-8.788 12.474 1.284 4.758 4.618 5.805 10.697 6.69 5.968.862 9.051.977 10.15 5.048.926 3.433-1.06 6.6-5.818 7.884-2.207.596-4.176.706-5.797.354l-.863-.661-2.11-5.482-2.894.781zM369.655 129.317l25.259-6.816-2.158-7.995-2.55.688.503 5.18-.475.97-13.144 3.546-3.706-13.733 8.044-2.17.885.55 1.443 3.4 2.305-.623-2.912-10.79-2.305.622.476 3.713-.501.872-8.044 2.17-3.348-12.408 11.33-3.058.884.55 1.812 3.984 2.746-.74-1.932-7.161-23.493 6.34.53 1.961 2.875-.46 1.019.462 7.293 27.024-.649.911-2.73 1zM388.2 89.006l2.037 7.553 2.698-.727-.544-4.747.488-.921 7.553-2.038 7.478 27.71-.698.925-3.22 1.132.543 2.011 12.801-3.454-.543-2.011-3.352.641-1.068-.448-7.478-27.71 7.553-2.039.885.55 1.918 4.376 2.697-.728-2.038-7.553z"/></g></svg>';
  }
?>

<?php // Generate styles specifically for this page
  $actionSlideFirst = field_get_items('node', $node, 'field_action_first')[0]['value'] === '1';
?>

<style>
  article.node-thank-you-page {
    background-image: url('<?php print $main_image_url; ?>');
    background-size: cover;
    background-attachment: fixed;
    background-position: center center;
  }
  article.node-thank-you-page section.article-body {
    padding-bottom: 0;
  }
</style>

<div id="OVERLAY GOES HERE">
<?php
  // Ensure overlay is printed out if required
  print render($content['campaignion_overlay_options']);
?>
</div>
<section class="article-body"<?php print $content_attributes; ?>>

  <section class="foe-scrolling-thank-you-page-section foe-scrolling-question">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="foe-scrolling-thank-you-page-section__inner">
            <header class="row article-header text-center">
              <div class="col-12">

                <?php if ( field_get_items('node', $node, 'field_show_title')[0]['value'] === '1' ): ?>
                  <h2 <?php print $title_attributes; ?>>
                    <?php if ( field_get_items('node', $node, 'field_public_title') ): 
                       print render($content['field_public_title']); 
                    else: 
                       print $title; 
                    endif ?>
                  </h2>                  
                <?php endif; ?>

                <?php if ( field_get_items('node', $node, 'field_subtitle') ): ?>
                  <h3><?php print render($content['field_subtitle']); ?></h3>
                <?php endif ?>

              <?php print render($title_suffix); ?>

              <?php if ( field_get_items('node', $node, 'body') ) : ?>
                <div class="scrolling-field-body">
                  <?php print render($content['body']); ?>
                </div>
              <?php endif; ?>
             </div>
              <div class="col-12 col-sm-6 offset-sm-3 mt-4">
                <div class="row">
                  <div class="col-12 col-sm-6 btn-container">
                    <button class="btn btn-primary btn-block" data-scroll-to="#slide-section-<?php echo $actionSlideFirst ? 'action' : 'share';?>" data-click-track="{&quot;eventCategory&quot;:&quot;interaction&quot;,&quot;eventAction&quot;:&quot;scrolling_thank_you_choice&quot;,&quot;eventLabel&quot;:&quot;yes&quot;}">Yes</button>
                  </div>
                  <div class="col-12 col-sm-6 btn-container">
                    <button class="btn btn-primary btn-block opacity-5" data-scroll-to="#slide-section-<?php echo $actionSlideFirst ? 'share' : 'action';?>" data-click-track="{&quot;eventCategory&quot;:&quot;interaction&quot;,&quot;eventAction&quot;:&quot;scrolling_thank_you_choice&quot;,&quot;eventLabel&quot;:&quot;no&quot;}">No</button>
                  </div>
                </div>
              </div>
            </header>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php
    // Assign action and share asks to first/second variables
    if ($actionSlideFirst):
      include(drupal_get_path('theme', 'foemorelesszen').'/templates/partials/thank-you/slides/action-slide.tpl.php');
      include(drupal_get_path('theme', 'foemorelesszen').'/templates/partials/thank-you/slides/share-slide.tpl.php');
    else:
      include(drupal_get_path('theme', 'foemorelesszen').'/templates/partials/thank-you/slides/share-slide.tpl.php');
      include(drupal_get_path('theme', 'foemorelesszen').'/templates/partials/thank-you/slides/action-slide.tpl.php');
    endif;
  ?>

</section>
<!-- END scrolling thank you page -->
