<?php
/*
 * @file partials/thank-you/default.tpl.php
 * @project foemorelesszen
 * @description This file contains the default partial for the thank you page, invoked from the node--thank-you-page.tpl 
 * 
 * @author Luigi Mannoni (hello@luigimannoni.com)
 * @created Wednesday, 4th July 2018 12:25:13 pm
 * 
 * @last-modified Wednesday, 4th July 2018 12:25:17 pm
 *                Luigi Mannoni (hello@luigimannoni.com)
 */
?>

<!-- Begin default thank you page -->
<section class="container-fluid">

  <header class="row article-header">
    <div class="col-12">

      <?php print render($title_prefix); ?>
      <h1<?php print $title_attributes; ?>>
      <?php if ( field_get_items('node', $node, 'field_public_title') ): 
            print render($content['field_public_title']); 
          else: 
            print $title; 
          endif ?>
      </h1>
      <?php if ( field_get_items('node', $node, 'field_subtitle') ): ?>
        <h2 class="subtitle"<?php print $title_attributes; ?>><?php print render($content['field_subtitle']); ?></h2>
      <?php endif ?>
      <?php print render($title_suffix); ?>

    </div>
  </header>

  <section class="article-body row"<?php print $content_attributes; ?>>
    <div class="col-12 col-sm-10 offset-sm-1 col-md-5 offset-md-1">
      <div class="field-body petition-copy">
        <?php
        // We hide the comments, tags and links now so that we can render them later.
        hide($content['field_main_image']);
        hide($content['comments']);
        hide($content['links']);
        hide($content['field_tags']);
        hide($content['field_campaign_type']);
        hide($content['recent_supporters']);
        hide($content['pgbar_default']);
        hide($content['webform']);
        hide($content['field_scrolling_active']);
        hide($content['field_action_first']);
        hide($content['field_first_ask_text']);
        hide($content['field_action_title']);
        hide($content['field_action_html']);
        hide($content['field_action_slide_embedded_form']);
        print render($content);
        ?>
      </div>
    </div>
    <aside class="col-12 col-sm-10 offset-sm-1 col-md-5 offset-md-0">
      <?php
      print render($content['field_main_image']);
      ?>
    </aside>
  </section>

</section>
<!-- END default thank you page -->