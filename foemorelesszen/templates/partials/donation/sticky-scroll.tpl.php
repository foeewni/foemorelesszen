<article data-nid="<?php print $node->nid; ?>" id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <section class="foe-new-ux-donation-2020">

    <div class="new-ux-sticky-container">

      <div class="article-header sticky-scroll-introduction" style="background-image:url(<?php echo $main_image_url; ?>);">
        <div class="container">
          <div class="row">
            <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-5 offset-lg-1 col-xl-6 offset-xl-0">

              <?php print render($content['pgbar_default']); ?>

              <?php print render($title_prefix); ?>
              <h1>
                <span class="display-1" <?php print $title_attributes; ?>><?php print render($content['field_public_title']); ?></span>
                <?php if ( field_get_items('node', $node, 'field_subtitle') ): ?>
                  <span class="display-2" <?php print $title_attributes; ?>><?php print render($content['field_subtitle']); ?></span>
                <?php endif ?>
              </h1>
              <?php print render($title_suffix); ?>

              <div class="field-body donation-copy">
                <?php print render($content['body']); ?>
              </div>

            </div>
          </div>
        </div>
      </div>

    </div>


    <?php /*
      This <aside> gets shifted to the bottom of the DOM after load
      so that the mobile (non-sticky) layout is correctly ordered.
      (Order doesn't matter when position: fixed is in place on desktop)
        >> See: foe.sticky-form.js
    */ ?>
    <aside id="sticky-form" class="new-ux-sticky-scroll">
      <div class="donation-sidebar container">
        <div class="row">
          <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-5 offset-lg-7">
            <div class="form-content">
              <div class="form-inner-content">
                <div class="prefill-salutation element-invisible">
                  <h3><span class="js-prefill-name"></span>, donate to the cause</h3>
                  <a href="#" class="js-prefill-reset">Not you?</a>
                </div>
                <?php
                  print render($content['webform']);
                ?>
                <div class="donation-certifications">
                  <?php include(drupal_get_path('theme', 'foemorelesszen').'/templates/partials/components/certification-logos-dark.tpl.php'); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </aside>


    <?php /*
      This scroll-to button (text and visibility) is driven from JS
       >> See: foe.sticky-form.js
    */ ?>
    <div class="new-ux-fixed-button text-center">
      <button class="d-inline-block" data-scroll-to=".new-ux-sticky-scroll"></button>
    </div>


  </section>

</article>
