<article data-nid="<?php print $node->nid; ?>" id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?> <?php if (isset($main_image_url)) { print 'style="background-image:url('.$main_image_url.')" '; } ?>>

  <?php $field_link_url = field_get_items('node', $node, 'field_link'); ?>
  <?php if ($field_link_url[0]['url']): ?>
    <?php hide($content['field_link']); ?>
    <section class="related-campaign-container">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <a href="<?php print $field_link_url[0]['url']; ?>">
              <?php print $field_link_url[0]['title']; ?>
            </a>
          </div>
        </div>
      </div>
    </section>
  <?php endif; ?>
      
  <section class="foe-donation-main-2017">
    <div class="bg-image-overlay"></div>
    <div class="container">

      <header class="article-header row">
        <div class="col-12">

          <?php print render($title_prefix); ?>
          <h1>
            <span class="display-1" <?php print $title_attributes; ?>>
            <?php if ( field_get_items('node', $node, 'field_public_title') ): 
              print render($content['field_public_title']); 
            else: 
              print $title; 
            endif ?>
            </span>
            <?php if ( field_get_items('node', $node, 'field_subtitle') ): ?>
              <span class="display-2" <?php print $title_attributes; ?>><?php print render($content['field_subtitle']); ?></span>
            <?php endif ?>
          </h1>
          <?php print render($title_suffix); ?>

        </div>
      </header>

      <?php if ($display_submitted): ?>
        <div class="submitted">
          <?php print $submitted; ?>
        </div>
      <?php endif; ?>
      <div class="row justify-content-center">
        <div class="donation-box col-12 col-sm-10 col-md-8 col-xl-4"<?php print $content_attributes; ?>>
          <div class="donation-box-inner">
            <?php
              hide($content['field_sticker']);
              hide($content['comments']);
              hide($content['links']);
              hide($content['body']);
              // print render($content);
              print render($content['pgbar_default']);
              print render($content['webform']);
            ?>
          </div>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="donation-certifications col-12 col-sm-10 col-md-8 col-xl-4">

          <div class="donation-certifications-inner d-flex flex-column flex-md-row">
            <?php include(drupal_get_path('theme', 'foemorelesszen').'/templates/partials/components/certification-logos.tpl.php'); ?>
          </div>

        </div>
      </div>

    </div>

    <?php if ( $content['field_sticker']): ?>
      <div class="donation-sticker">
        <?php print render($content['field_sticker']); ?>
      </div>
    <?php endif; ?>

    <a data-scroll-to="#content_bottom_anchor" class="content-arrow-slide arrow_slide">Slide down</a>
  </section>

</article>