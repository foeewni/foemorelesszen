<section id="foe-quiz-action-slide" class="quiz-slide quiz-slide-end d-none">
  <div class="container">
    <div class="row">
      <div class="col-12">
          <div class="row align-items-center">
            <div class="col-12 col-lg-6 foe-quiz-action-text">
              <h6><?php print t('Nearly done...'); ?></h6>
              <?php print render($content['field_action_html']); ?>
            </div>

            <div class="col-12 col-lg-6">

              <?php if(!empty(field_get_items('node', $node, 'quiz_linked_form')[0]['target_id'])): ?>
              <?php
                $node_embed = node_load(field_get_items('node', $node, 'quiz_linked_form')[0]['target_id']);
                $ty_pages = field_get_items('node', $node_embed, 'field_thank_you_pages');

                foreach ($ty_pages as $ty_page) {
                  if (isset($ty_page['node_reference_nid']) && !empty($ty_page['node_reference_nid'])) {
                    $ty_page_nid = $ty_page['node_reference_nid'];
                  }
                }

                // Build our path to the thank you page
                $ty_url = array(
                  drupal_get_path_alias("node/{$ty_page_nid}"), // Outputs the alias
                  "?share=node/{$node_embed->nid}", // Share target (campaignion default)
                  "&sid=false", // Campaignion sends also submissions ids but we don't have one 
                  "&quiz={$node->nid}" // Our reference to the quiz
                );

                $ty_url = implode($ty_url, '');
              ?>

              <section class="foe-quiz-action-inner">
                <div class="foe-quiz-action-pre row mb-4">
                  <div class="col-12">
                    <h4><?php print render($content['field_action_title']); ?></h4>
                  </div>
                </div>
                <div class="foe-quiz-action-pre row justify-content-center">
                  <div class="col-8">
                    <button foe-quiz-action-accept class="btn btn-accent btn-rounded btn-block"><?php print t('Yes, of course'); ?></button>
                    <a href="/<?php echo $ty_url; ?>" class="btn btn-outline-offwhite btn-rounded btn-block"><?php print t('No, sorry'); ?></a>
                  </div>
                </div>
                <div class="foe-quiz-action-content row align-items-center">
                  <div class="col-12 quiz-override node-<?php print $node_embed->type; ?>">
                    <?php
                      webform_node_view($node_embed, 'full');
                      print theme_webform_view($node_embed->content);
                    ?>
                  </div>
                </div>
              </section>
              <?php endif; ?>

          </div>
        </div>
      </div>    
    </div>
  </div>
</section>