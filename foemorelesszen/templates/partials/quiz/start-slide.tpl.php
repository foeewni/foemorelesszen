<section class="quiz-slide quiz-slide-start">
  <div class="container">
    <div class="row justify-content-center mb-4">
      <div class="col-12 text-center">
        <?php print render($title_prefix); ?>
          <h2<?php print $title_attributes; ?>>
          <?php if ( field_get_items('node', $node, 'field_public_title') ): 
            print render($content['field_public_title']); 
          else: 
            print $title; 
          endif ?>
          </h2>
        <?php print render($title_suffix); ?>        
        <?php
          hide($content['quiz_config']);
          hide($content['quiz_linked_form']);
          hide($content['field_action_title']);
          hide($content['field_action_html']);
          print render($content);
        ?>      
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="<?php echo $quiz_classes; ?>">
      <button id="foe-quiz-start" class="btn btn-outline-donate btn-rounded btn-block">
          <span id="foe-quiz-loader-bar" style="width:0%"></span>
          <span id="foe-quiz-loader-step">Loading</span>  
      </button>
    </div>    
  </div>
</section>  