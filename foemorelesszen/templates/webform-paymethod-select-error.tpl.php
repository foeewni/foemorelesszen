<?php

/**
 * @file
 * Render an error message for previous payment attempts.
 *
 * Available variables:
 * - $payment: The payment that’s being processed.
 * - $status: The current payment status.
 * - $status_title: The title of the current status.
 */
?>
<?php echo t('The payment attempt has failed with a status of "!status_title". Please try again.', ['!status_title' => $status_title]); ?>
