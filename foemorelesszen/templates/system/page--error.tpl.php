<?php

/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/garland.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content_top']: Items for the header region.
 * - $page['content']: The main content of the current page.
 * - $page['content_bottom']: Items for the header region.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 */
?>

<?php // FOE Custom header ?>
<?php include(drupal_get_path('theme', 'foemorelesszen').'/templates/components/main-header.tpl.php'); ?>


<section class="foe-main" role="main">
  <article class="error-page">
  <div class="container">
      <header class="article-header row">
        <div class="col-12">
          <?php $header = drupal_get_http_header('status'); ?>
          <?php if (stripos($header, '404') !== FALSE): ?>
            <h1 class="display-1"><?php echo t('404'); ?></h1>
            <h2 class="display-2"><?php echo t('Page not found'); ?></h2>
          <?php else: ?>
            <h1 class="display-1"><?php echo t('403'); ?></h1>
            <h2 class="display-2"><?php echo t('Access denied'); ?></h2>
          <?php endif; ?>
        </div> 
      </header>
    </div>
    <section class="pt-4 pb-4">
      <div class="container">
        <div class="row">
          <div class="col-12 col-sm-10 offset-sm-1 col-md-6 offset-md-0">
            <?php if (stripos($header, '404') !== FALSE): ?>
              <h3>Uh oh, it seems we’ve been a little overzealous with our recycling!</h3>
              <p>The content you've requested could not be found.</p>
            <?php else: ?>
              <h3>We’re sorry, you don’t have access to this page.</h3>
              <p>You may need to sign in, or sign in with a different account.</p>
              <a class="btn btn-accent btn-block" href="/user">Sign in</a>          
            <?php endif; ?>
          </div>
        </header>
      </div>
    </section>
  </article>
</section>


<?php if($page['footer']): ?>
  <footer class="foe-footer" role="contentinfo">
    <div class="<?php echo $container_class; ?>">
      <div class="row">
        <div class="col-12">
          <?php print render($page['footer']); ?>    
          <div class="customer-line d-block d-sm-none">
            <?php print render($page['header']); ?>
          </div>
        </div>        
      </div>
    </div>
  </footer>
<?php endif; ?>