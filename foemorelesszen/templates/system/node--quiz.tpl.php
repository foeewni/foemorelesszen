<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */


  $field_background_image = field_get_items('node', $node, 'field_main_image');
    
  if ($field_background_image) {
    $style_name = 'background_image';
    $image_uri = $field_background_image[0]['uri'];
    $derivative_uri = image_style_path($style_name, $image_uri);
    $success = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style_name), $image_uri, $derivative_uri);
    $main_image_url  = file_create_url($derivative_uri);
  }

  $params = drupal_get_query_parameters();
  $quiz_json = field_get_items('node', $node, 'quiz_config')[0]['value'];

  $quiz_slides = json_decode($quiz_json);
  $quiz_slides_total = count($quiz_slides);

  // $quiz_taken = (isset($_COOKIE["quiz-{$node->nid}"]) && !isset($params['restart']));

  $quiz_classes = 'col-12 col-md-10 col-lg-8 col-xl-6 text-center';
  $classes .= ' d-flex flex-column align-items-top bg-extradark';
  // $classes .= (!$quiz_taken) ? ' bg-extradark' : ' bg-offwhite quiz-ended';

?>


<article foe-quiz data-nid="<?php print $node->nid; ?>" id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <i class="foe-quiz-bg" style="background-image:url(<?php echo $main_image_url; ?>)"></i>
  <header id="foe-quiz-score" class="container">
    <div class="row">
      <div class="logo">
        <img class="foe-logo foe-logo-white" src="/<?php echo drupal_get_path('theme', 'foemorelesszen'); ?>/img/logo/foe-logo-horizontal.png">
        <img class="foe-logo foe-logo-negative" src="/<?php echo drupal_get_path('theme', 'foemorelesszen'); ?>/img/logo/foe-logo-negative-horizontal.png">
      </div>
    </div>
    <div class="row justify-content-center quiz-only">
      <p class="col-12 text-center">
        <span id="foe-quiz-progress-step">1</span> <?php print t('of @total', array('@total' => $quiz_slides_total)); ?>
      </p>
      <div class="<?php echo $quiz_classes; ?>">
        <div class="progress bg-transparent">
          <div id="foe-quiz-progress-bar" class="progress-bar progress-bar-animated bg-accent" style="width:0%"></div>
        </div>
      </div>
    </div>
  </header>

  <main id="foe-quiz-main" class="quiz-container d-flex align-items-top align-items-lg-center">
    <?php include(drupal_get_path('theme', 'foemorelesszen').'/templates/partials/quiz/start-slide.tpl.php'); ?>
    
    <?php foreach ($quiz_slides as $id => $slide): ?>
    <?php include(drupal_get_path('theme', 'foemorelesszen').'/templates/partials/quiz/question-slide.tpl.php'); ?>
    <?php endforeach; ?>

    <?php include(drupal_get_path('theme', 'foemorelesszen').'/templates/partials/quiz/end-slide.tpl.php'); ?>
  </main>

</article>