<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: whether submission information should be displayed.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<?php if ($page): ?>
<?php $classes .= ' node-petition-sidebar'; ?>
<?php
  $streamlined_display = !isset($content['body']) || empty(trim($content['body']['#items'][0]['value']));
  // If there's no body copy, we'll use the image as a background (if provided) and center the form.

  if ($streamlined_display) {

    $field_main_image = field_get_items('node', $node, 'field_main_image');

    if ($field_main_image && $field_main_image[0]['uri'] != 'video') {
      $style_name = 'background_image';
      $image_uri = $field_main_image[0]['uri'];
      $derivative_uri = image_style_path($style_name, $image_uri);
      $success = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style_name), $image_uri, $derivative_uri);
      $main_image_url = file_create_url($derivative_uri);

      $classes .= ' streamlined-display-with-background';

    }

  }

?>

<article data-nid="<?php print $node->nid; ?>" id="node-<?php print $node->nid; ?>" <?php if (isset($main_image_url)) { echo 'style="background-image:url(' . $main_image_url . '" '; } ?> class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <?php if (isset($main_image_url)): ?>
    <div class="bg-image-overlay"></div>
  <?php endif; ?>

  <div class="container">
    <header class="row article-header">
      <div class="col-12">
    
        <?php print render($title_prefix); ?>
        <h1<?php print $title_attributes; ?>>
        <?php if ( field_get_items('node', $node, 'field_public_title') ): ?>
            <?php print render($content['field_public_title']); ?>
          <?php else: ?>
            <?php print $title; ?>
          <?php endif ?>
        </h1>
        <?php if ( field_get_items('node', $node, 'field_subtitle') ): ?>
          <h2 class="subtitle"<?php print $title_attributes; ?>><?php print render($content['field_subtitle']); ?></h2>
        <?php endif ?>
        <?php print render($title_suffix); ?>      

      </div>
    </header>


    <section class="article-body row"<?php print $content_attributes; ?>>

      <?php
        if ($streamlined_display):
      ?>
          <div class="col-12 col-sm-10 offset-sm-1 col-md-6 offset-md-3">
            <div class="petition-sidebar">
              <?php
              print render($content['pgbar_default']);
              print render($content['webform']);

              $supporters_active = field_get_items('node', $node, 'recent_supporters')[0]['toggle'];
              if ($supporters_active == 1): ?>
                <hr>
                <div class="petition-supporters">
                  <h3><?php print t('Our supporters'); ?></h3>
                  <?php print render($content['recent_supporters']); ?>
                </div>
              <?php endif; ?>
            </div>
          </div>

      <?php
        else: // Not $streamlined_display
      ?>

          <div class="col-12 col-sm-10 offset-sm-1 col-md-6 offset-md-0">
            <div class="field-body petition-copy">
              <?php
              print render($content['field_main_image']);
              print render($content['body']);
              ?>
            </div>
          </div>
          <aside class="col-12 col-sm-10 offset-sm-1 col-md-5">
            <div class="petition-sidebar">
              <?php
              print render($content['pgbar_default']);
              print render($content['webform']);
              ?>
            </div>
            <?php
            global $base_url;
            $node_absolute_url = $base_url . $node_url;
            ?>

            <?php $supporters_active = field_get_items('node', $node, 'recent_supporters')[0]['toggle']; ?>
            <?php if ($supporters_active == 1): ?>
              <div class="petition-supporters">
                <h3><?= t('Our supporters'); ?></h3>
                <?php
                print render($content['recent_supporters']);
                ?>
              </div>
            <?php endif ?>
          </aside>

      <?php
        endif;
      ?>

    </section>

  </div>

  
  <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
    <footer>
      <?php print render($content['field_tags']); ?>
      <?php print render($content['links']); ?>
    </footer>
  <?php endif; ?>

  <?php print render($content['comments']); ?>

</article><!-- /.node -->

<?php else: ?>

<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
    
  <div class="container">

    <div class="row">
      <div class="col-12 col-sm-6"<?php print $content_attributes; ?>>
        <?php print render($title_prefix); ?>
          <h2<?php print $title_attributes; ?>><a class="node-link" href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
          <?php if ($content['field_subtitle']): ?>
            <h3 class="subtitle"<?php print $title_attributes; ?>><?php print render($content['field_subtitle']); ?></h3>
          <?php endif ?>          
        <?php print render($title_suffix); ?> 

        <?php if ($display_submitted): ?>
          <div class="submitted">
            <?php print $submitted; ?>
          </div>
        <?php endif; ?>
        <div class="field-body">
          <?php
            hide($content['field_main_image']);
            hide($content['comments']);
            hide($content['links']);
            hide($content['field_tags']);
            hide($content['field_tags']);
            hide($content['field_campaign_type']);
            hide($content['recent_supporters']);
            hide($content['pgbar_default']);
            hide($content['webform']);
            print render($content['pgbar_default']);
            print render($content);                   
          ?>          
        </div>
      </div>

      <div class="col-12 col-sm-6"<?php print $content_attributes; ?>>
          <?php print render($content['field_main_image']);  ?>
      </div>
    </div>

  </div>

</article>

<?php endif; ?>





<?php
// Just throwing this snippet here for safekeeping, although it seems
// unlikely that we'll want to insert these back in as-is...

/* Decomment when campaignion team want social buttons back
          <h6>Share online</h6>
          <ul class="share-buttons list-inline">
            <li>
              <a href="https://www.facebook.com/sharer/sharer.php?u=<?= urlencode($node_absolute_url); ?>&t=<?= $title; ?>" title="Share on Facebook" target="_blank">
                <i class="fa fa-facebook"></i>
              </a>
            </li>
            <li>
              <a href="https://twitter.com/intent/tweet?source=<?= urlencode($node_absolute_url); ?>&text=<?= $title; ?>:%20<?= urlencode($node_absolute_url); ?>&via=wwwfoecouk" target="_blank" title="Tweet">
                <i class="fa fa-twitter"></i>
              </a>
            </li>
            <li>
              <a href="http://www.tumblr.com/share?v=3&u=<?= urlencode($node_absolute_url); ?>&t=<?= $title; ?>&s=" target="_blank" title="Post to Tumblr">
                <i class="fa fa-tumblr"></i>
              </a>
            </li>
            <li>
              <a href="http://pinterest.com/pin/create/button/?url=<?= urlencode($node_absolute_url); ?>&description=<?= $title; ?>" target="_blank" title="Pin it">
                <i class="fa fa-pinterest"></i>
              </a>
            </li>
            <li>
              <a href="mailto:?subject=<?= $title; ?>&body=<?= urlencode($node_absolute_url); ?>" target="_blank" title="Send email">
                <i class="fa fa-envelope"></i>
              </a>
            </li>
          </ul>
*/
?>
