<?php

/**
 * @file
 * Customize the display of a complete webform.
 *
 * This file may be renamed "webform-form-[nid].tpl.php" to target a specific
 * webform on your site. Or you can leave it "webform-form.tpl.php" to affect
 * all webforms on your site.
 *
 * Available variables:
 * - $form: The complete form array.
 * - $nid: The node ID of the Webform.
 *
 * The $form array contains two main pieces:
 * - $form['submitted']: The main content of the user-created form.
 * - $form['details']: Internal information stored by Webform.
 */
?>
<?php
  if(isset($form['submitted']['title1'])){
    print $form['submitted']['title1']['#markup'];
  }
  if(isset($form['submitted']['title2'])){
    print $form['submitted']['title2']['#markup'];
  }
  if(isset($form['submitted']['title3'])){
    print $form['submitted']['title3']['#markup'];
  }
  if(isset($form['submitted']['title4'])){
    print $form['submitted']['title4']['#markup'];
  }
  
  // Print webform steps first
  if (isset($form['webform_steps']))
    print drupal_render($form['webform_steps']);
  if (isset($form['progressbar'])) {
    print drupal_render($form['progressbar']);
  }
  // Print out the main part of the form.
  // Feel free to break this up and move the pieces within the array.
  print drupal_render($form['submitted']);

  // Always print out the entire $form. This renders the remaining pieces of the
  // form that haven't yet been rendered above.
  print drupal_render_children($form);
?>