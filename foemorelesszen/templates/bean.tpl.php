<?php
/**
 * @file
 * Default theme implementation for beans.
 *
 * Available variables:
 * - $content: An array of comment items. Use render($content) to print them all, or
 *   print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $title: The (sanitized) entity label.
 * - $url: Direct url of the current entity if specified.
 * - $page: Flag for the full page state.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity-{ENTITY_TYPE}
 *   - {ENTITY_TYPE}-{BUNDLE}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
 $bean_id_class = '';
 if(in_array('bean-full-width-image', $classes_array) && isset($content['field_button']['#object']->field_full_width_image)){
   $bean_id_class = 'bean-id-' . $content['field_button']['#object']->bid;
   $style_name = 'background_image';
   $image_uri = $content['field_button']['#object']->field_full_width_image[LANGUAGE_NONE][0]['uri'];
   $derivative_uri = image_style_path($style_name, $image_uri);
   //$img = drupal_realpath(image_style_url($style_name, $img_path));
   $success        = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style), $image_uri, $derivative_uri);
   $new_image_url  = file_create_url($derivative_uri);
   ?>
   <style type="text/css" media="all">
    body .region-content-bottom <?php print '.block-bean-full-width-image .bean-id-' . $content['field_button']['#object']->bid; ?>{
      background-color:#FFFFFF !important;
      background-image:url('<?php print $new_image_url;?>') !important;
      background-repeat:no-repeat !important;
      background-attachment:static !important;
      background-position:center center !important;
      background-size:cover !important;
      -webkit-background-size:cover !important;
      -moz-background-size:cover !important;
      -o-background-size:cover !important;
    }
    </style>
   <?php
 }
?>
<div class="<?php print $classes . ' ' . $bean_id_class; ?> clearfix"<?php print $attributes; ?>>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      print render($content);
    ?>
  </div>
</div>
