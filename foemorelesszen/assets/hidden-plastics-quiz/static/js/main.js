$(document).ready(function(){

  var quizUrl = Drupal.absoluteUrl('/act/hidden-plastics-quiz');
  var thanksUrlQuizOnly = Drupal.absoluteUrl('/thanks/hidden-plastics-quiz');
  // Not used here // var thanksUrlPetition = Drupal.absoluteUrl('/thanks/hidden-plastics-petition');
  var sharePageUrl = Drupal.absoluteUrl('/share/hidden-plastics-quiz');
  var donateUrl = Drupal.absoluteUrl('/donate/plastic-free?source=FN1808001');

	var score = 0;
	var scoreString = '';

	var questions = [
		{
			question: 'So, which of these often contains hidden plastic?',
			answers: [{text: 'Chewing gum'},{text: 'Unpackaged processed cheese'},{text: 'Loose rice'}],
			correctAnswer: 0,
			userAnswer: 'X',
			solutionText: 'Chewing gum contains plastic. (Rice and cheese can too if they’re kept in plastic packaging, but plastic isn’t intentionally added to them when they made).'
		},
		{
			question: 'Which of these often contains plastic?',
			answers: [{text: 'Paper clips'},{text: 'Newspapers'},{text: 'Teabags'}],
			correctAnswer: 2,
			userAnswer: 'X',
			solutionText: 'Teabags are often sealed with heated plastic'
		},
		{
			question: 'Which of these often contains plastic?',
			answers: [{text: 'Foil'},{text: 'Egg boxes'},{text: 'Cartons'}],
			correctAnswer: 2,
			userAnswer: 'X',
			solutionText: 'Cartons have a plastic coating'
		},
		{
			question: 'Which of these often contains plastic?',
			answers: [{text: 'Glass bottles'},{text: 'Enamel mugs'},{text: 'Paper cups (without lids)'}],
			correctAnswer: 2,
			userAnswer: 'X',
			solutionText: 'Paper cups also have a plastic coating'
		},
		{
			question: 'Which of these often contains plastic?',
			answers: [{text: 'Bricks'},{text: 'Crisp packets'},{text: 'Glass jars'}],
			correctAnswer: 1,
			userAnswer: 'X',
			solutionText: 'Crisp packets have a plastic film inside'
		},
		{
			question: 'Which of these often contains plastic?',
			answers: [{text: 'Light bulbs'},{text: 'Matches'},{text: 'Tote bags'}],
			correctAnswer: 0,
			userAnswer: 'X',
			solutionText: 'Light bulbs (most modern ones use plastic)'
		},
		{
			question: 'Which of these often contains plastic?',
			answers: [{text: 'Porcelain plates'},{text: 'Wet wipes'},{text: 'Coloured soaps'}],
			correctAnswer: 1,
			userAnswer: 'X',
			solutionText: 'Wet wipes are made of plastic resins, not tissue paper'
		},
		{
			question: 'Which of these often contains plastic?',
			answers: [{text: 'Wooden cutlery'},{text: 'Cardboard food boxes'},{text: 'Paper bags'}],
			correctAnswer: 1,
			userAnswer: 'X',
			solutionText: 'Cardboard food boxes are often coated with plastic'
		}
	];


  var writeCookie = function() {

		var dt, expires;
		dt = new Date();
		dt.setTime(dt.getTime()+(1*24*60*60*1000));
		expires = ", expires="+dt.toGMTString();

		var website_host = window.location.hostname;
		document.cookie = "quiz="+scoreString+"; path=/;domain=."+website_host;
	}

	var getCookie = function(cname) {

		var name = cname + "=";
		var decodedCookie = decodeURIComponent(document.cookie);
		var ca = decodedCookie.split(';');
		for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return "";

  }
  
	function scrollTo (element) {
    $('html, body').animate({scrollTop: element.offset().top}, 800);
    
	}


  

  // Move to next slide
	$('.skip').click(function() {

		var $target = $(this).closest('.slide').next('.slide');
		if ($(this).data("target")) {
			var targetid = $(this).data("target");
			var $target = $('#'+targetid);
		}

	  $target.show();
		scrollTo($target);

	});

	// Calculate score and write score string
	var calcScore = function() {
		var tempscore = 0;
		scoreString = '';
		questions.forEach(function (question) {
			if(question.userAnswer == question.correctAnswer){
				tempscore ++;
			}
			scoreString += question.userAnswer.toString();
		});
		score = tempscore;
		return;
	}

	// Save user answer and calculate score
	$('.question-card-option').click(function(){
		var q = $(this).data("q");
		var a = $(this).data("a");
		questions[q].userAnswer = a;
		calcScore();
		// Remove selected class from other answers and set this div as selected
		$(this).closest('.questions-container').find('.question-card-option').removeClass('selected');
		$(this).addClass('selected');
		writeCookie();
		return;
	});

	$('.score-breakdown-container .question-card').click(function(){
		$(this).find('.question-card-overlay').fadeToggle();
		$(this).toggleClass('opened');
	});

	//Set score breakdown
	var setScoreBreakdown = function() {
		scoreString = getCookie('quiz');
		scoreString = scoreString.replace(/x/gi, '0');
    scoreString = scoreString.replace(/^@/gi, ''); // Remove the prefix if we reload the results page for some reason

		var thisscore = 0;
    if (scoreString) {
      questions.forEach(function (question, index) {
        //For each question get the div by id
        var thiscard = $('#score'+index);
        //Reset classes
        $(thiscard).removeClass('right');
        $(thiscard).removeClass('wrong');
        //Get user answer
        var thisUserAnswer = scoreString[index];
        //If answer is correct
        if(thisUserAnswer == question.correctAnswer){
          thisscore++;
          $(thiscard).addClass('right');
          //Set overlay text
          $(thiscard).find('.question-card-overlay .copy').html(question.solutionText);
        } else {
          $(thiscard).addClass('wrong');
          //Set overlay text
          var youGuessed = question.answers[thisUserAnswer].text;
          var youGuessedText = '<div class="youguessed">(You guessed '+youGuessed+')</div>';
          $(thiscard).find('.question-card-overlay .copy').html(question.solutionText + youGuessedText);
        }
      });
      score = thisscore;
    } else {
		  score = '???';
    }
    
    $('#slide-score .text-title-deco').text('You scored '+score+" out\u00A0of\u00A0"+questions.length);
    
    // They've hit the results page -- add a prefix to the cookie so if they go back they can try again
    scoreString = '@' + scoreString;
    writeCookie();

	}










  // Set share preview text based on score and variant
  // NOTE: v3 was the winner, other variants are now disabled, but kept here for reference.
	var setShareText = function() {
		sharetitle = 'Can you tell where the hidden plastics are in your home? Find out >>';
		sharedesc = 'Hardly anyone knows about all of these – do you? Take the test.';
		if(shareCopyVariant == 1){
			sharetitle = 'Can you tell where the hidden plastics are in your home? Find out >>';
			sharedesc = 'Hardly anyone knows about all of these – do you? Take the test.';
		} else if (shareCopyVariant == 2) {
			sharetitle = 'Are you using plastic without knowing? Find out now >>';
			sharedesc = 'Hardly anyone knows about all of these – do you? Take the test';
		} else if (shareCopyVariant == 3) {
			sharetitle = 'Are you using plastic without knowing? I got '+score+' out of 8 right >>';
			sharedesc = 'Think you can do better? Take the test.';
		}
		$('.share-preview-title').text(sharetitle);
		$('.share-preview-description').text(sharedesc);
	}

	
	$('.btn-part2').click(function(){
		window.location = (thanksUrlQuizOnly);
	});

  // Not sure why this was done instead of just using links?
	// $('.buttons-container-donate .btn-primary').click(function(){
	// 	var amount = $(this).data('amount');
	// 	window.open(donateUrl+'#p:select='+amount, this);
	// });


	//Share on FB
	$('.btn-facebook').click(function(){
		var toShareUrl = encodeURIComponent(sharePageUrl+'?quizdata='+score+'/'+shareCopyVariant);
		var shareURL = 'https://www.facebook.com/sharer/sharer.php?u='+toShareUrl;
		window.open(shareURL, '_blank', 'toolbar=no,location=0,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=250,top=300,left=300');
	})

	//Share on Twitter
	$('.btn-twitter').click(function(){
		var here = quizUrl;
		if(shareCopyVariantWhatsapp == 1){
			var text = 'Hi – I just took this hidden plastics quiz '+here+' – plastic’s in so much stuff! See if you can beat my score – and check out the action at the end. 🐠🐬 #PlasticFreeFuture'; 
		} else if(shareCopyVariantWhatsapp == 2) {
			var text = 'Hi – if you want to save our sealife 🐠🐬 and use less plastic in your life, you have to take this test! '+here+' I had no idea about some of these hidden plastics – see if you can do better than me! #PlasticFreeFuture';
		}
		var sharetext = encodeURIComponent(text);
		var shareURL = 'https://twitter.com/intent/tweet?text='+sharetext;
		window.open(shareURL, '_blank');
	})

	//Share on Whatsapp
	$('.btn-whatsapp').click(function(){
		var here = quizUrl;
		var text = 'Just took this test on everyday hidden plastics '+here+' – can you get them all? Check out the end to see why you should share too.';
		var sharetext = encodeURIComponent(text);
		var shareURL = 'whatsapp://send?text='+sharetext;
		window.open(shareURL, '_blank');
	})

	//Share via Email
	$('.btn-email').click(function(){
		var here = quizUrl;
		var esubject = 'Can you spot the hidden plastics?';
		var ebody = 'Hello. I just took this quiz and thought I’d share and see if you can do better than me. '+here+' I had no idea about some of these hidden plastics – it\'s in so many things! Check out the action at the end too and see help stop the tide of deadly throwaway plastics.';
		window.open('mailto:?subject='+esubject+'&body='+encodeURIComponent(ebody));
	})


  // ----- Initialisation -----
  
  $('.slide:eq(0)').show(); // We always show the first slide

  // Load answers from cookie, show and scroll past already-answered slides
  if ($(document.body).hasClass('node-type-petition')) {

    var initialScoreString = getCookie('quiz');
    var currentAnswerCount = 0;

    if (initialScoreString[0] === '@') {
      // Reset -- we've already seen the thank you page
      initialScoreString = initialScoreString.replace(/^@/gi, '');
      initialScoreString = initialScoreString.replace(/./gi, 'X');
    }

    for (; currentAnswerCount < initialScoreString.length ; currentAnswerCount++) {
      if (initialScoreString[currentAnswerCount] !== 'X') {

        // Update the 'questions' data object
        questions[currentAnswerCount].userAnswer = initialScoreString[currentAnswerCount];

        // Set this answer as selected
        $('div.question-card[data-q=' + currentAnswerCount + '][data-a=' + initialScoreString[currentAnswerCount] + ']').addClass('selected');

        // Show the next slide
        $('.slide:eq(' + (currentAnswerCount+1) + ')').show();

      } else {
        break;
      }
    }

    if (currentAnswerCount > 0 && currentAnswerCount == initialScoreString.length) {
      // We've already got all answers, most likely situation is that it was
      // a form submission error, so show all slides and scroll to the form.
      $('.slide').show();
      scrollTo($('#slide-form'));
    } else {
      scrollTo($('.slide:eq(' + currentAnswerCount + ')'));
    }
  }


	if ($(document.body).hasClass('node-type-thank-you-page')) {
  	setScoreBreakdown();
    $('.gotoshare').click(function(){
      setShareText();
    });
    setShareText();
	}

});
