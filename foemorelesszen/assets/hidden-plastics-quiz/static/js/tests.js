function analytics(action, label) {
  if (window.gtag && typeof window.gtag === "function") {
    gtag('event', action, {
      'event_category': 'foe-plastic',
      'event_label': label
    });
  }
}

/*
var shareChannelsTest = new ABTester({
  cookieName: 'FAShareChannelsTest',
  experimentName: 'share-channels-test',
  variants: [
    {
      name: 'facebook',
      callback: function () {
        analytics('channel test', 'facebook');
        app.tests.extraShareChannel = 'none';
      },
    },
    {
      name: 'facebook-twitter',
      callback: function () {
        analytics('channel test', 'facebook-twitter');
        app.tests.extraShareChannel = 'twitter';
      },
    },
    {
      name: 'facebook-whatsapp',
      callback: function () {
        analytics('channel test', 'facebook-whatsapp');
        app.tests.extraShareChannel = 'whatsapp';
      },
    },
    {
    name: 'facebook-email',
      callback: function () {
        analytics('channel test', 'facebook-email');
        app.tests.extraShareChannel = 'email';
      },
    },
  ]
});
*/

/* // Disable variation on the petition ask copy

var petitionSlideTest = new ABTester({
  cookieName: 'FAPetitionSlideTest',
  experimentName: 'petition-slide-test',
  variants: [
    {
      name: 'v1',
      callback: function () {
        //analytics('share copy test', 'v1');
        $('.slide-petition-variants').css('background-image','url(/sites/all/themes/foemorelesszen/assets/hidden-plastics-quiz/static/img/bg2.jpg)');
        $('.slide-petition-variants-title').text('While we check your answers, do you have a few seconds to help stop plastic being hidden in so many of the things we use?');
        $('.slide-petition-variants-text1').text('We all want more plastic-free choices - but manufacturers are still adding unnecessary coatings, films and wraps.');
        $('.slide-petition-variants-text2').hide();
        $('.slide-petition-variants-title-mobile').text('While we check your answers ...');
        $('.slide-petition-variants-cta').text('Tell the government: “clamp down on needless throwaway plastics for good”. Will you sign our petition?');
      },
    },
    {
      name: 'v2',
      callback: function () {

      },
    }
  ]
});
*/

var shareChannelsTest = new ABTester({
  cookieName: 'FAShareChannelsTest',
  experimentName: 'share-channels-test',
  variants: [
    {
      name: 'v1',
      callback: function () {
        $('.share-buttons .btn-twitter').hide();
        $('.share-buttons .btn-email').hide();
        $('.share-buttons .btn-whatsapp').hide();
      },
    },
    {
      name: 'v2',
      callback: function () {
        $('.share-buttons .btn-twitter').hide();
        $('.share-buttons .btn-email').hide();
      },
    },
    {
      name: 'v3',
      callback: function () {
        $('.share-buttons .btn-whatsapp').hide();
        $('.share-buttons .btn-email').hide();
      },
    },
    {
      name: 'v4',
      callback: function () {
        $('.share-buttons .btn-whatsapp').hide();
        $('.share-buttons .btn-twitter').hide();
      },
    }
  ]
});


// NOTE: v3 was the winner, split test is now disabled. 
var shareCopyVariant = 3;

// var shareCopyTest = new ABTester({
//   cookieName: 'FAShareCopyTest',
//   experimentName: 'share-copy-test',
//   variants: [
//     {
//       name: 'v1',
//       callback: function () {
//         //analytics('share copy test', 'v1');
//         shareCopyVariant = 1;
//       },
//     },
//     {
//       name: 'v2',
//       callback: function () {
//         shareCopyVariant = 2;
//       },
//     },
//     {
//       name: 'v3',
//       callback: function () {
//         shareCopyVariant = 3;
//       },
//     }
//   ]
// });

var shareCopyVariantWhatsapp = 1;

var shareCopyTest = new ABTester({
  cookieName: 'FAShareCopyWhatsappTest',
  experimentName: 'share-copy-whatsapp-test',
  variants: [
    {
      name: 'v1',
      callback: function () {
        shareCopyVariantWhatsapp = 1;
      },
    },
    {
      name: 'v2',
      callback: function () {
        shareCopyVariantWhatsapp = 2;
      },
    }
  ]
});
