import { TimelineLite } from 'gsap/TweenMax';

export default {

  accept: function accept() {
    const $ = window.jQuery;

    // Text outside the blue action container
    const $text = $('.foe-quiz-action-text');
    // The pre-referenced node/form text inside the blue container
    const $pre = $('.foe-quiz-action-pre');
    // Blue container
    const $inner = $('.foe-quiz-action-inner');
    // The referenced node/form action
    const $content = $('.foe-quiz-action-content');

    let contentHeight = 0;

    // Get size helpers from CSS vars
    // with manual fallback
    const breakpoint = parseInt(window.getComputedStyle(document.head).getPropertyValue('--breakpoint-lg'), 10) || 1024;

    // Our base GSAP Animation
    const tl = new TimelineLite({ paused: true, align: 'normal', delay: 0 });
    const duration = 0.5;

    tl
      .to([$text, $pre], duration, {
        opacity: 0,
      })
      .add('text-hidden')
      .to($content, duration, {
        opacity: 1,
        onStart: () => {
          // Reset the normal flow of the elements,
          // slap a class so we can drive it on CSS and through
          // breakpoints
          $inner.css('height', '').addClass('gsap-done');
          $content.attr('style', '');
          $pre.addClass('d-none');
        },
      }, `text-hidden+=${duration * 2}`); // Delay otherwise the height animation overlaps with onStart

    // Next we attach a different translate/width animation at the
    // 'text-hidden' label on GSAP because mobiles are stacked each other on the columns
    // while the desktop expands for both columns (width)
    if (window.innerWidth < breakpoint) {
      // We're in mobile mode, so we have a different animation
      tl.to($inner, duration, {
        y: $text.outerHeight() * -1,
        onComplete: () => {
          // We get the outer height (h + padding) for the action so we
          // can transition the blue inner container
          contentHeight = $content.outerHeight();
          tl.to($inner, duration, {
            height: contentHeight,
          }, `text-hidden+=${duration}`);
        },
      }, 'text-hidden');
    } else {
      // Animation for desktop mode
      tl.to($inner, duration, {
        x: '-50%',
        onStart: () => {
          // Retain the height before zooming otherwise it jumps
          $inner.css('height', $inner.outerHeight());
        },
        onComplete: () => {
          // We get the outer height (h + padding) for the action so we
          // can transition the blue inner container
          contentHeight = $content.outerHeight();
          tl.to($inner, duration, {
            height: contentHeight,
          }, `text-hidden+=${duration}`);
        },
      }, 'text-hidden');
    }

    tl.play();
  },

  reject: function reject() {

  },
};
