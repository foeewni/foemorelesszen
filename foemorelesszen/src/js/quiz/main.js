// I wish I had angular already loaded in on the page
// I'm going full ES6, expect javascript communicating through events

import QuizUtils from './quizutils.service';
import QuizSlide from './quizslide.service';
import QuizAction from './quizaction.service';
import QuizThanks from './quizthanks.service';
import Cookie from '../utils/cookie.service';

// Init our FoeQuiz global
window.FoeQuiz = {
  id: null,
  status: null,
  time: {
    start: null,
    end: null,
  },
  questions: {
    current: 0,
    total: null,
  },
  answers: [],
  data: null,
  $ui: {},
  thanks: [],
};

(($, Drupal, Q) => {
  const init = {
    game: function game() {
      // Grab our json
      Q.data = Drupal.settings.quiz.questions;
      // Set our total questions
      Q.questions.total = Q.data.length;

      // Init our base elements, canvas, slides and the background
      Q.$ui.canvas = $('article[foe-quiz]');
      Q.$ui.slides = Q.$ui.canvas.find('.foe-quiz-slide');
      Q.$ui.bg = Q.$ui.canvas.find('.foe-quiz-bg');
      Q.$ui.bg.img = Q.$ui.bg.css('background-image');

      // Store our quiz ID
      Q.id = Q.$ui.canvas.attr('data-nid');

      // Init our loader elements
      Q.$ui.loader = {};
      Q.$ui.loader.button = $('#foe-quiz-start');
      Q.$ui.loader.bar = $('#foe-quiz-loader-bar');
      Q.$ui.loader.step = $('#foe-quiz-loader-step');

      QuizUtils.preload();

      // Init our game progress elements
      Q.$ui.progress = {};
      Q.$ui.progress.bar = $('#foe-quiz-progress-bar');
      Q.$ui.progress.step = $('#foe-quiz-progress-step');

      // Init the last step elements
      Q.$ui.action = {};
      Q.$ui.action.slide = $('#foe-quiz-action-slide');
      Q.$ui.action.form = $('#foe-quiz-action-slide').find('form');

      // Sort out whether we can jump straight to the petition
      if (Cookie.get('quiz-' + Drupal.settings.quiz.id) !== '') {
        // We have a cookie! Let's blindly assume we can jump to the petition
        Q.$ui.action.slide.removeClass('d-none');
        Q.$ui.action.slide.find('.foe-quiz-action-text').addClass('d-none d-lg-block d-opacity-0');
        Q.$ui.action.slide.find('.foe-quiz-action-inner').addClass('gsap-done');
        Q.$ui.action.slide.find('.foe-quiz-action-pre').addClass('d-none');
        Q.$ui.canvas.find('section.quiz-slide-start').addClass('d-none');
        Q.$ui.canvas.removeClass('bg-extradark').addClass('bg-offwhite quiz-ended');
      }

      // Init our event handlers

      // Main Foe Quiz events
      $(document).on('foequiz:progress', QuizUtils.progress);
      $(document).on('foequiz:slide.next', QuizSlide.next);
      $(document).on('foequiz:start', QuizUtils.start);
      $(document).on('foequiz:end', QuizUtils.end);

      // Quiz game
      $(document).on('click', '[foe-quiz-answer]', QuizUtils.answer);

      // Action buttons
      $(document).on('click', '[foe-quiz-action-accept]', QuizAction.accept);
      $(document).on('click', '[foe-quiz-action-reject]', QuizAction.reject);
    },

    thanks: function thanks() {
      Q.data = Drupal.settings.quiz.questions;
      Q.questions.total = Q.data.length;

      Q.$ui.canvas = $('article[foe-quiz]');
      Q.$ui.challenge = $('#challenge-cta');

      Q.id = Q.$ui.canvas.attr('data-quiz');

      QuizThanks.init();
      QuizThanks.recalculate();

      $(window).on('resize', QuizThanks.recalculate);
      $(window).on('scroll', QuizThanks.watch);
      $(document).on('click', '[foe-thanks-reveal]', QuizThanks.reveal);
    },
  };

  // Inits on window load
  $(window).bind('load', () => {
    // Init game if we're in the quiz page
    if ($('body').hasClass('node-type-quiz')) {
      init.game();
    }

    // Init thank you if we're in the quiz thank you page
    if ($('body').hasClass('node-type-thank-you-quiz')) {
      init.thanks();
    }
  });
})(window.jQuery, window.Drupal, window.FoeQuiz);
