(function($) {
  Drupal.behaviors.foe_more_toggle = {};
  Drupal.behaviors.foe_more_toggle.attach = function(context, settings) {

    $('.more-toggle', context).each(function(){
      var $toggle = $(this);
      var $target = $($toggle.attr('href'));
      var readLessText = $toggle.attr('data-read-less-text') || 'Show less';
      var originalText = $toggle.html();

      $target.slideUp(0);

      $toggle.on('click', function(e) {

        if ($target.hasClass('expanded')) {
          $target.removeClass('expanded').slideUp();
          $toggle.html( originalText );
        }
        else {
          $target.addClass('expanded').slideDown();
          $toggle.html( readLessText );
        }
        
        e.preventDefault();
      });

    });

  };
})(jQuery);
