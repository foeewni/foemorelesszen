/**
 * @file paypal_button_customize.js
 */


(function ($) {

  Drupal.behaviors.foe_paypal_button_customize = {
    attach: function(context) {
      $('.payment-method-form.form-wrapper', context).find('.fieldset-legend').each(function(){
        var $this = $(this); 

        // Check for existence
        if ($this.html().indexOf('Paypal') != -1) {
          // Add custom class
          $this.parent().addClass('legend-paypal');
        }
        
      });
  
    }
  };

})(jQuery);