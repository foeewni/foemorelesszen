(function ($, Drupal) {
  'use strict';

  // References for re-use
  var isSticky, overlay, formContents;

  // Gap between header/top of viewport and footer/bottom of viewport
  var formVerticalSpacing = 32; // px

  var focusForm = function () {
    if (isSticky) {
      $(overlay).addClass('active');
      $(formContents).addClass('active');
    }
  };

  var blurForm = function () {
    $(overlay).removeClass('active');
    $(formContents).removeClass('active');
  };

  var updatePageLayout = function () {
    var headerHeight = $('header.foe-header').outerHeight();
    var mainPageContentElement = $('section.foe-main');
    mainPageContentElement.css('margin-top', headerHeight + 'px');
  };

  var updateFormPositioning = function () {

    isSticky = $(window).width() >= 1024; // Hardcoded breakpoint :/

    // If we're not in sticky mode (meaning we're in mobile view)
    // just reset any applied top margin and max height styles.
    if (!isSticky) {
      formContents.style.marginTop = null;
      formContents.style.maxHeight = null;
      return;
    }

    // ...Otherwise, calculate the form's size and positioning
    var headerHeight = $('header.foe-header').outerHeight();
    var footerOffset = $('footer.foe-footer').offset().top;

    var formInnerContentHeight = $('.form-inner-content').outerHeight();
    var firstPanelHeight = $('.new-ux-sticky-container').outerHeight();
    var centeredFormMarginTop = headerHeight + (firstPanelHeight/2 - formInnerContentHeight/2);

    var scrollDist = $(document).scrollTop();
    var viewportHeight = window.innerHeight;

    var marginTop = headerHeight + formVerticalSpacing;
    var marginBottom = formVerticalSpacing;

    // Prevent header overlap, keeping the form centered within the top
    // panel until it hits the header if we have an especially tall viewport
    if (scrollDist < centeredFormMarginTop) {
      marginTop = Math.max(centeredFormMarginTop - scrollDist, marginTop);
    }

    // Prevent footer overlap
    if (scrollDist + viewportHeight > footerOffset) {
      marginBottom = (scrollDist + viewportHeight - footerOffset) + formVerticalSpacing;
    }

    // Apply calculated margins to the element via marginTop and maxHeight
    formContents.style.marginTop = marginTop + 'px';
    formContents.style.maxHeight = 'calc( 100vh - ' + (marginTop + marginBottom) + 'px)';
  };

  Drupal.behaviors.foe_sticky_form = {

    attach: function (context) {

      $('body', context).once( function () {

        // We don't need to do anything here unless we have a specific <body> class
        if (!$(this).hasClass('layout-sticky-scroll')) {
          return;
        }

        // DOM manipulation: move the sticky form to end of
        // section.foe-main so the mobile layout is correct.
        var $stickyForm = $('#sticky-form');
        $('section.foe-main').append($stickyForm);

        // Add the overlay element to .region-content
        overlay = document.createElement('div');
        overlay.id = 'overlay';
        $(this).prepend(overlay);

        // Set up reference to the form-content element
        formContents = $('#sticky-form .form-content')[0];

        // Add event listeners so that clicks on form "focus" the form,
        // and clicks on the new overlay defocus the form
        $(overlay).on('click', blurForm);
        $stickyForm.on('click', focusForm);

        // Calculate position and max-height of the form when scrolling or any layout change occurs
        $(window, context).on('scroll resize load orientationchange', function () {

          // Ensure the body margin matches the current fixed header height
          updatePageLayout();

          // Position and keep the form from exceeding the viewport height (plus a buffer)
          updateFormPositioning();

          // Hide the overlay (by "defocussing" the form) if we're not in sticky mode
          if ($(window).width() < 1024) {
            blurForm();
          }

        });

      });
    }
  };

})(jQuery, Drupal);
