export default {
  getScrollTop: function getScrollTop() {
    if (typeof window.pageYOffset !== 'undefined') {
      // most browsers except IE before #9
      return window.pageYOffset;
    }
    const B = document.body; // IE 'quirks'
    let D = document.documentElement; // IE with doctype
    D = (D.clientHeight) ? D : B;
    return D.scrollTop;
  },
  getHalfScrollTop: function getHalfScrollTop() {
    return exports.default.getScrollTop() + (window.innerHeight / 2);
  },
  getViewportHeight: function getViewportHeight() {
    return Math.max(document.documentElement.clientHeight, window.innerHeight);
  },
};
