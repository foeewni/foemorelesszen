// Copied straight from the original Quiz

export default {

  set: function set(cname = null, content = null, days = 3600000) {
    if (cname === null || content === null) {
      throw new Error('Cookie name and or content cannot be null, please set them in the function arguments');
    }
    const encCookie = encodeURIComponent(JSON.stringify(content));
    const dt = new Date();
    dt.setTime(dt.getTime() + (1 * 24 * days));
    document.cookie = `${cname}=${encCookie}; path=/; domain=.${window.location.hostname}; expires=${dt.toGMTString()}`;
  },

  get: function get(cname) {
    const name = `${cname}=`;
    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return '';
  },
};
