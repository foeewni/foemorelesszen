/**
 * @file foe_custom_event_tracking_helper.js
 *
 * Creates an agnostic helper function Foe.trackCustomEvent() to trigger tracking events across multiple tracking
 * platforms. By default it will send the event to all defined platforms, but an optional second argument can provide
 * a whitelist as an array of strings. "Whitelisted" platforms that do not exist will be silently ignored.
 *
 * Add platform function definitions to the 'platforms' object as needed, taking the eventName as a string parameter
 * and returning boolean true or false, indicating success or failure.
 */

(function (Foe, $) {
  'use strict';

  var maxRetries = 5;
  var retryDelay = 2000; // Milliseconds


  // The platforms, returning true on success
  var platforms = {

    googleAnalytics: function (eventName) {
      if (typeof window['ga'] == 'function') {
        ga('send', eventName);
        return true;
      } else {
        return false;
      }
    },

    facebookPixel: function (eventName) {
      if (typeof window['fbq'] == 'function') {
        fbq('trackCustom', eventName);
        return true;
      } else {
        return false;
      }
    },

  };


  // The helper function
  Foe.trackCustomEvent = function (eventName, platformWhitelist, attempts) {
    attempts = attempts || 0;
    var platformList = platformWhitelist || Object.getOwnPropertyNames(platforms);

    for (var i = 0; i < platformList.length; i++) {
      if (typeof platforms[platformList[i]] === 'function') {

        // Retry this platform if unsuccessful and we haven't reached maxRetries
        if (!platforms[platformList[i]](eventName) && attempts < maxRetries) {
          window.setTimeout(function(a, b, c) {
            Foe.trackCustomEvent(a, b, c);
          }.bind(null, eventName, [platformList[i]], attempts+1),
          retryDelay);
        };

      }
    }

  };

})(window.Foe = window.Foe || {}, jQuery);
