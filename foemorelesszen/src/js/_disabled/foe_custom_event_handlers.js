/**
 * @file foe_custom_event_handlers.js
 *
 * Fires events based on certain conditions. Only universal events should be
 * tracked here -- others should be inserted per-action basis (via add-to-head? GTM?).
 *
 * Add event triggers as needed, following the pattern and ensuring events are
 * only tracked when guaranteed to be correct. (Avoid false positives!)
 *
 * Relies on the Foe.trackCustomEvent() helper function, defined in
 * js/foe_custom_event_tracking_helper.js
 *
 */

// TODO: Look into de-duping/preventing double-firing on refresh/ensuring it's a "real" thank you event?
// TODO: ...Probably not an issue though.

(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.foe_custom_event_handlers = {};
  Drupal.behaviors.foe_custom_event_handlers.attach = function (context, settings) {

    // Abort if helper function is not found
    if (typeof Foe === 'undefined' || typeof Foe.trackCustomEvent !== 'function') {
      console.log('Friends of the Earth helper function not available, skipping custom event tracking.');
      return;
    }



    // -------------------------------------------------
    // Track data protection events from thank-you pages
    // -------------------------------------------------

    if ($('body', context).hasClass('node-type-thank-you-page')) {

      var channels = ['email', 'telephone', 'post'];
      var channel, channelSetting;

      for (var i = 0; i < channels.length; i++) {

        channel = channels[i];
        channelSetting = JSON.parse(sessionStorage.getItem('webform_prefill:l:' + channel + '_opt_in'));

        if (channelSetting && channelSetting.length === 1) {

          Foe.trackCustomEvent('dp_' + channel + '_' + channelSetting[0].toLowerCase());

        }

      }

    }


    // ------------------------------------------------
    // Track giftaid opt-in events from thank-you pages
    // ------------------------------------------------

    if ($('body', context).hasClass('node-type-thank-you-page')) {

      var giftaid_checkboxes = JSON.parse(sessionStorage.getItem('webform_prefill:l:gift_aid'));

      if (giftaid_checkboxes && giftaid_checkboxes.length && giftaid_checkboxes.indexOf("gift_aid") !== -1) {

        Foe.trackCustomEvent('giftaid_opt_in');

      }

    }




  }; // End Drupal.behaviors.foe_custom_event_handlers.attach()

})(jQuery, Drupal);