<?php

/**
 * @see hook_preprocess_page().
 */
function foemorelesszen_preprocess_page (&$vars) {
  global $base_url;

  // TODO: Get this working behind Amazee pygmy proxy
  // Append livereload js if we are in local
  // if (strpos($base_url, 'foe-impact-stack-legacy.docker.amazee.io') !== FALSE) {
  //   drupal_add_js('http://foe-impact-stack-legacy.docker.amazee.io:9001/livereload.js', 'external');
  // }

  // Custom Error page for 404 and 403 only
  $header = drupal_get_http_header('status');
  if (stripos($header, '404') !== FALSE || stripos($header, '403') !== FALSE) {
    $vars['theme_hook_suggestions'][] = 'page__error';
  }

}
