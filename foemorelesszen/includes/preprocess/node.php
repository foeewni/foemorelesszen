<?php

function foemorelesszen_preprocess_node (&$variables) {
  $node = $variables['node'];

  if( $node->type == 'donation' && empty($variables['field_sticker']) ) {
    unset($variables['content']['field_sticker']);
  }

  // For quizzes. load the JSON as an object on the javascript Drupal.settings variable
  if( $node->type == 'quiz' && !empty($variables['quiz_config']) ) {
    drupal_add_js(array(
      'quiz' => array(
        'id' => $node->nid,
        'questions' => json_decode($variables['quiz_config'][0]['value'])
      )
    ), 'setting');
  }

  $params = drupal_get_query_parameters();
  if ( $node->type == 'thank_you_page' && isset($params['quiz']) ) {
    $node_quiz = node_load($params['quiz']);
    $quiz_json = field_get_items('node', $node_quiz, 'quiz_config')[0]['value'];
    drupal_add_js(array(
      'quiz' => array(
        'id' => $node_quiz->nid,
        'questions' => json_decode($quiz_json)
      )
    ), 'setting');
  }

  if ( $node->type == 'thank_you_page' && $node->share_light['und'][0]['toggle'] == '1') {
    include('node.thank-you-page.share-slide.php');
  }

  if ($node->type === 'thank_you_page' && isset($params['prefill'])) {
    $prefillData = explode('|', base64_decode($params['prefill']));
    $nid = $prefillData[0];
    $urn_hash = $prefillData[1];

    $prefillSettings = _foemorelesszen_prefill_get_settings($nid);

    drupal_add_js(
      array(
        'prefillEndpoint' => $prefillSettings['endpoint'],
        'prefillSubmit' => array(
          'nid' => $nid,
          'chk' => $prefillSettings['hash'],
          'urn' => $urn_hash,
        ),
      ), 'setting');
  }

}
