<?php

function foemorelesszen_preprocess_field (&$variables) {

  $element = $variables['element'];

  switch($element['#field_name']) {

    case 'field_cta':
    // Add BTN classes to the links contained in field_cta
      $variables['items'] = array();
      foreach ($element['#items'] as $delta => $item) {
        if (!empty($element[$delta])) {
          $element[$delta]['#element']['attributes']['class'] = 'btn btn-donate btn-lg';
          $variables['items'][$delta] = $element[$delta];
        }
      }
    break;

    case 'campaignion_overlay_options':
    // Add a wrapper to the overlay output
      //$variables['items'][0]['#markup'] = 'XXXX';
        // '<div class="pants">' .
        // $variables['items'][0]['#markup'] .
        // '</div>';
    break;

    case 'campaignion_overlay_introduction':
    case 'field_public_title':
    // Do submission token replacement on the overlay introduction text / public title

      // Gather available webform submission information
      $sid = (isset($_GET['sid']) && is_numeric($_GET['sid'])) ? (int) $_GET['sid'] : 0;
      $hash = isset($_GET['hash']) ? $_GET['hash'] : NULL;
      $webform_nid = (isset($_GET['share']) && preg_match('|^node/\d+$|', $_GET['share'])) ? (int) str_replace('node/', '', $_GET['share']) : NULL;

      // Get the submission data (if we have enough information)
      $submission = _campaignion_webform_tokens_get_submission($sid, $hash);

      // If we've got a submission and a webform node ID we can do token
      // replacement later, so load in the webform node.
      if ($submission && $webform_nid) {
        $webform_node = node_load($webform_nid);
      }

      // Replace webform tokens in the text
      if (isset($webform_node) && isset($submission) ) {
        $variables['items'][0]['#markup'] = webform_replace_tokens(
          $variables['items'][0]['#markup'],
          $webform_node,
          $submission
        );
      }

    break;

    default:
    break;

  }

}
