<?php

function _foemorelesszen_prefill_get_settings($hashing_id) {
  global $base_url;

  // Add prefill settings
  $prefill_endpoint = 'https://service-prefill-dev.azurewebsites.net'; // Dev Endpoint
  $salt = 'Qq54j$5pT@jUM7$^SgM%ZcSDqQay7B*D'; // Dev Hash
  if ((strpos($base_url, 'foeuk.staging.campaignion.org') !== FALSE) || (strpos($base_url, 'act.foe.co.uk') !== FALSE) || (strpos($base_url, 'act.friendsoftheearth.uk') !== FALSE)) {
    // This is the prod endpoint.
    $prefill_endpoint = 'https://service-prefill-prod.azurewebsites.net';
    // This is the prod salt.
    $salt = 'QKUCzKu8y$!dC7mFwZYn77$J6!f3@FZhb%^AT?v=7H%LLy3eM^FYabJL2L3jw2r%9YZ2Vsy_YajA*@EFn-_!2^?sR*jbf6!bM65MtMW#G+RQpMHTb*G^R^cQ!*#9rGA?';
  }

  // Note: $form_id is the machine name, $element['#id'] is the ID on markup (has hyphens instead of underscores)
  // We use the markup to get the form ID on the JS side
  $check_hash = hash_hmac('sha512', $hashing_id, $salt);
  return array(
    'endpoint' => $prefill_endpoint,
    'hash' => $check_hash,
  );
}

/**
 * @see hook_form_element()
 * https://drupal.stackexchange.com/questions/44323/adding-form-placeholders-to-text-input-fields
 * https://api.drupal.org/api/drupal/developer%21topics%21forms_api_reference.html/7.x
 */

function foemorelesszen_form_alter(&$form, &$form_state, $form_id) {

  // Tidy-up all forms to apply our styling
  _foemorelesszen_add_attrs($form);

  // Add prefill details only if we are in a webform generated form
  if ($form['#type'] === 'form' && stripos($form_id, 'webform_client_form') === 0) {
    $params = drupal_get_query_parameters();

    // Note: $form_id is the machine name, $form['#node']->nid is the Node ID
    // Form id is flaky and changes in few occasions, we use the NID on the JS/Prefill side to store submissions
    $prefill = _foemorelesszen_prefill_get_settings($form['#node']->nid);
    drupal_add_js(array('prefillEndpoint' => $prefill['endpoint']), 'setting');

    $check_hash = $prefill['hash'];
    $user_storage = isset($form_state['user']) ? $form_state['user'] : null;
    $urn_hash = isset($params['user']) ? $params['user'] : $user_storage;


    // Sync PHP and JS.
    if (isset($urn_hash)) {
      $form['#attributes']['hash'] = $check_hash;
      $form['#attributes']['nid'] = $form['#node']->nid;
      $form['#attributes']['user'] = $urn_hash;

      $form['prefill_session'] = array(
        '#type' => 'hidden',
        '#value' => 'true',
      );

      if (!isset($form_state['user'])) {
        $form_state['user'] = $urn_hash;
      }
    }

    // Add prefill userdata if we're in session
    /*
      $url = $prefill['endpoint'] . '/api/v1/form/prefill';
      $prefill_params = 'hash=' . $urn_hash . '&nid=' . $form['#node']->nid  . '&chk=' . $check_hash;

      $ch = curl_init( $url );
      curl_setopt( $ch, CURLOPT_POST, 1);
      curl_setopt( $ch, CURLOPT_POSTFIELDS, $prefill_params);
      curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt( $ch, CURLOPT_HEADER, 0);
      curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt( $ch, CURLOPT_TIMEOUT_MS, 3000);

      $response = curl_exec( $ch );
      $prefill_response = json_decode($response);
      drupal_add_js(array('prefillResponse' => $prefill_response), 'setting');
      if (isset($prefill_response) && $prefill_response->status === 200) {
        $form['#attributes']['user'] = $urn_hash;
        $form['#attributes']['hash'] = $check_hash;
        $form['#attributes']['nid'] = $form['#node']->nid;

        // Add an hidden field. We use this to sync sessions
        // between php handler and js.
        $form['prefill_session'] = array(
          '#type' => 'hidden',
          '#value' => 'true',
        );

        if (!isset($form_state['user'])) {
          $form_state['user'] = $urn_hash;
        }
      }
    }
    */

  }

}

/**
 * Custom helper function to loop elements and childrens inside form elements
 */

function _foemorelesszen_add_attrs(&$element) {
  if (isset($element['#type'])) {
    switch ($element['#type']) {
      case 'textarea':
      case 'textfield':
      case 'webform_number':
      case 'webform_email':
      case 'password':
      case 'password_confirm':
      case 'postcode':
        $element['#attributes']['class'][] = 'form-control';
        if (!isset($element['#attributes']['placeholder']) || $element['#attributes']['placeholder'] == '') {
          $element['#attributes']['placeholder'] = $element['#title'];
        }
        break;

      case 'select_or_other':
        if (!isset($element['#attributes']['placeholder']) || $element['#attributes']['placeholder'] == '') {
          $element['#attributes']['placeholder'] = $element['#title'];
        }
        $element['#attributes']['data-select-or-other-hide'] = 'true';
        break;

      case 'submit':
        $classes_array = array('btn', 'btn-block');

        // Replace classes on donate buttons
        if (stripos($element['#value'], 'donate') !== FALSE) {
          $classes_array[] = 'btn-donate';
        } else {
          $classes_array[] = 'btn-accent';
        }

        // Strip everything if the button is originally a step-button
        if (isset($element['#attributes']['class']) && in_array('step-button', $element['#attributes']['class'])) {
          $classes_array = array('step-button');
        }

        $element['#attributes']['class'] = $classes_array;
      break;
    }


  }

  if ($element && element_children($element)){
    foreach (element_children($element) as $key) {
      _foemorelesszen_add_attrs($element[$key]);
    }
  }

}


/**
 * @see theme_form_element()
 */
function foemorelesszen_form_element(&$variables) {
  $element = &$variables['element'];

  $name = !empty($element['#name']) ? $element['#name'] : FALSE;
  $type = !empty($element['#type']) ? $element['#type'] : FALSE;
  $wrapper = isset($element['#form_element_wrapper']) ? !!$element['#form_element_wrapper'] : TRUE;
  $form_group = isset($element['#form_group']) ? !!$element['#form_group'] : $wrapper && $type && $type !== 'hidden';
  $checkbox = $type && $type === 'checkbox';
  $radio = $type && $type === 'radio';

  // Create an attributes array for the wrapping container.
  if (empty($element['#wrapper_attributes'])) {
    $element['#wrapper_attributes'] = array();
  }
  $wrapper_attributes = &$element['#wrapper_attributes'];

  // This function is invoked as theme wrapper, but the rendered form element
  // may not necessarily have been processed by form_builder().
  $element += array(
    '#title_display' => 'before',
  );

  // Add wrapper ID for 'item' type.
  if ($type && $type === 'item' && !empty($element['#markup']) && !empty($element['#id'])) {
    $wrapper_attributes['id'] = $element['#id'];
  }

  // Check for errors and set correct error class.
  if ((isset($element['#parents']) && form_get_error($element) !== NULL)) {
    $wrapper_attributes['class'][] = 'field-error';
  }

  // Add necessary classes to wrapper container.
  $wrapper_attributes['class'][] = 'form-group';
  $wrapper_attributes['class'][] = 'pristine';
  if ($name) {
    $wrapper_attributes['class'][] = 'form-group-' . drupal_html_class($name);
  }
  if ($type) {
    $wrapper_attributes['class'][] = 'form-type-' . drupal_html_class($type);
  }
  if (!empty($element['#attributes']['disabled'])) {
    $wrapper_attributes['class'][] = 'form-disabled';
  }
  if (!empty($element['#autocomplete_path']) && drupal_valid_path($element['#autocomplete_path'])) {
    $wrapper_attributes['class'][] = 'form-autocomplete';
  }

  // Add a helper class if the element is required
  // loose comparison doesn't work for some reason (?)
  if ($element['#required'] === '1' || $element['#required'] === TRUE) {
    $wrapper_attributes['class'][] = 'field-is-required';
  }

  // Checkboxes and radios do no receive the 'form-group' class, instead they
  // simply have their own classes.
  if ($checkbox || $radio) {
    $wrapper_attributes['class'][] = drupal_html_class($type);
  }
  elseif ($form_group) {
    $wrapper_attributes['class'][] = 'form-group';
  }

  // Create a render array for the form element.
  $build = array(
    '#form_group' => $form_group,
    '#attributes' => $wrapper_attributes,
  );

  if ($wrapper) {
    $build['#theme_wrappers'] = array('container__form_element');

    // Render the label for the form element.
    $build['label'] = array(
      '#markup' => theme('form_element_label', $variables),
      '#weight' => $element['#title_display'] === 'before' ? 0 : 2,
    );
  }

  // Checkboxes and radios render the input element inside the label. If the
  // element is neither of those, then the input element must be rendered here.
  if (!$checkbox && !$radio) {
    if ($type == 'textfield' &&
          (strpos(drupal_html_class($name), 'donation-amount-other') !== FALSE ||
           strpos(drupal_html_class($name), 'dd-amount-other') !== FALSE ||
           strpos(drupal_html_class($name), 'regular-amount-other') !== FALSE)) {
      $element['#field_prefix'] = '£';
      $element['#input_group'] = TRUE;
      $element['#input_group_button'] = FALSE;
    }

    $prefix = isset($element['#field_prefix']) ? $element['#field_prefix'] : '';
    $suffix = isset($element['#field_suffix']) ? $element['#field_suffix'] : '';


    if ((!empty($prefix) || !empty($suffix)) && (!empty($element['#input_group']) || !empty($element['#input_group_button']))) {
      if (!empty($element['#field_prefix'])) {
        $prefix = '<span class="input-group-text input-group--' . (!empty($element['#input_group_button']) ? 'btn' : 'addon') . '">' . $prefix . '</span>';
      }
      if (!empty($element['#field_suffix'])) {
        $suffix = '<span class="input-group-text input-group--' . (!empty($element['#input_group_button']) ? 'btn' : 'addon') . '">' . $suffix . '</span>';
      }

      // Add a wrapping container around the elements.
      // $input_group_attributes = &_foemorelesszen_get_attributes($element, 'input_group_attributes');
      $input_group_attributes = array('class' => array('input-group'));
      $input_group_attributes_pre = array('class' => array('input-group-prepend'));
      $input_group_attributes_app = array('class' => array('input-group-append'));
      $prefix = '<div' . drupal_attributes($input_group_attributes) . '>' . '<div' . drupal_attributes($input_group_attributes_pre) . '>' . $prefix;
      $suffix .= '</div></div>';
    }

    // Build the form element.
    $build['element'] = array(
      '#markup' => $element['#children'],
      '#prefix' => !empty($prefix) ? $prefix : NULL,
      '#suffix' => !empty($suffix) ? $suffix : NULL,
      '#weight' => 1,
    );
  }

  // Construct the element's description markup.
  if (!empty($element['#description'])) {
    $build['description'] = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array('help-block'),
      ),
      '#weight' => isset($element['#description_display']) && $element['#description_display'] === 'before' ? 0 : 20,
      0 => array('#markup' => filter_xss_admin($element['#description'])),
    );
  }

  // Render the form element build array.
  return drupal_render($build);
}

/**
 * @see theme_form_element_label()
 */
function foemorelesszen_form_element_label(&$variables) {
  $element = $variables['element'];

  // Extract variables.
  $output = '';

  $title = !empty($element['#title']) ? filter_xss_admin($element['#title']) : '';

  // Only show the required marker if there is an actual title to display.
  if ($title && $required = !empty($element['#required']) ? theme('form_required_marker', array('element' => $element)) : '') {
    $title .= ' ' . $required;
  }

  $display = isset($element['#title_display']) ? $element['#title_display'] : 'before';
  $type = !empty($element['#type']) ? $element['#type'] : FALSE;
  $checkbox = $type && $type === 'checkbox';
  $radio = $type && $type === 'radio';

  // Immediately return if the element is not a checkbox or radio and there is
  // no label to be rendered.
  if (!$checkbox && !$radio && ($display === 'none' || !$title)) {
    return '';
  }

  // Retrieve the label attributes array.
  // $attributes = &_foemorelesszen_get_attributes($element, 'label_attributes');

  // Add Bootstrap label class.
  $attributes['class'] = array('control-label');

  // Add the necessary 'for' attribute if the element ID exists.
  if (!empty($element['#id'])) {
    $attributes['for'] = $element['#id'];

    if (stripos($attributes['for'], 'paymethod-select-payment-method-selector') !== FALSE) {
      $attributes['class'][] = 'btn';
      $attributes['class'][] = 'btn-primary';
      $attributes['class'][] = 'btn-block';
    }
  }

  // Checkboxes and radios must construct the label differently.
  if ($checkbox || $radio) {
    if ($display === 'none' || $display === 'invisible') {
      $output_title = '<span class="element-invisible">' . $title . '</span>';
    }
    else {
      $output_title = $title;
    }
    // Build our label
    $label = '<label' . drupal_attributes($attributes) . '>' . $output_title . '</label>';

    // Inject the rendered checkbox or radio element inside the output.
    if (!empty($element['#children'])) {
      $input = $element['#children'];
    } else {
      $input = '';
    }

    if ($display === 'before') {
      $output .= $label . $input;
    }
    if ($display === 'after') {
      $output .= $input . $label;
    }

    // For checkboxes and radios we render the input before the label.
    return $output;
  }
  // Otherwise, just render the title as the label.
  else {
    // Show label only to screen readers to avoid disruption in visual flows.
    if ($display === 'invisible') {
      $attributes['class'][] = 'element-invisible';
    }
    $output .= $title;

    // The leading whitespace helps visually separate fields from inline labels.
    return ' <label' . drupal_attributes($attributes) . '>' . $output . "</label>\n";
  }
}

/**
 * @see theme_webform_element()
 */
function foemorelesszen_preprocess_webform_element(&$variables) {
  $element = $variables['element'];
  $wrapper_attributes = array();
  if (isset($element['#wrapper_attributes'])) {
    $wrapper_attributes = $element['#wrapper_attributes'];
  }

  // See http://getbootstrap.com/css/#forms-controls.
  if (isset($element['#type'])) {
    if ($element['#type'] === 'radio') {
      $wrapper_attributes['class'][] = 'radio';
    }
    elseif ($element['#type'] === 'checkbox') {
      $wrapper_attributes['class'][] = 'checkbox';
    }
    elseif ($element['#type'] !== 'hidden') {
      $wrapper_attributes['class'][] = 'form-group';
    }
  }

  $variables['element']['#wrapper_attributes'] = $wrapper_attributes;
}

/**
 * Returns HTML for a webform element.
 *
 * @see theme_webform_element()
 * @see foemorelesszen_form_element()
 */
function foemorelesszen_webform_element(&$variables) {
  $element = &$variables['element'];

  // Inline title.
  if (isset($element['#title_display']) && $element['#title_display'] === 'inline') {
    $element['#title_display'] = 'before';
    $element['#wrapper_attributes']['class'][] = 'form-inline';
  }

  // Description above field.
  if (!empty($element['#webform_component']['extra']['description_above'])) {
    $element['#description_display'] = 'before';
  }

  // If field prefix or suffix is present, make this an input group.
  if (!empty($element['#field_prefix']) || !empty($element['#field_suffix'])) {
    $element['#input_group'] = TRUE;
  }

  // Render as a normal "form_element" theme hook.
  return theme('form_element', $variables);
}



/**
 * Implements hook_form_BASE_FORM_ID_alter() for webform_client_form().
 *
 * Insert custom submit handler.
 */
function foemorelesszen_form_webform_client_form_alter(&$form, &$form_state) {

  // Load the current node (logic snitched from core)
  $node = menu_get_object();

  // Append a redirect handler if the submission comes from a quiz node
  if ($node && $node->type == 'quiz') {
    // Store our current quiz nid on the form
    $form_state['quiz_nid'] = $node->nid;
    $form['#submit'][] = '_foemorelesszen_quiz_redirect';
  }


  // Append a redirect handler if the submission has a prefill session
  if (isset($form_state['user'])) {
    // We're submitting a prefilled form, can be recognised from the user state
    // we injected earlier. Should work on any form

    // Set transitional data on the form state
    $form_state['form_id'] = $form['#id'];
    $form_state['nid'] = $node->nid;

    // Gotcha moment: this has to be placed as last handler to get the webform_completed state sucessfully
    $form['#submit'][] = '_foemorelesszen_prefill_redirect';
  }
}

/**
 * Display a message to a user if they are not allowed to fill out a form.
 *
 * @param array $variables
 *   The variables array.
 */
function foemorelesszen_webform_view_messages(array &$variables) {
  $node = $variables['node'];
  $page = $variables['page'];
  $submission_count = $variables['submission_count'];
  $cached = $variables['cached'];
  $type = 'status';
  if ( ($node->webform['submit_interval'] == -1 && $node->webform['submit_limit'] == 1) ||
       ($submission_count > 0 && $node->webform['submit_notice'] == 1 && !$cached) ) {
    if (empty($message)) {
      $message = t('It looks like you’ve already taken part in this action – thank you for your support! Why not check out <a href="!url">some other campaigns</a> that you can get involved with?', array('!url' => url('https://friendsoftheearth.uk/latest/actions')));
    }
  }
  if ($page && isset($message)) {
    drupal_set_message($message, $type, FALSE);
  }
}

/**
 * Form-submit callback for webform_client_form in quiz mode.
 */
function _foemorelesszen_quiz_redirect($form, &$form_state) {
  // Prevents locking campaignion ajax responses
  if (!$form_state['webform_completed'] || !$form_state['redirect']) {
    return;
  }

  $redirect = $form_state['redirect']; // Should be already normalized from campaignion
  $redirect[1]['query']['quiz'] = $form_state['quiz_nid']; // Append the quiz nid on the redirect
  $form_state['redirect'] = $redirect;
}

/**
 * Form-submit callback for webform_client_form if it is in prefilled state.
 */
// SERVER-SIDE PREFILL DISABLED
// (Call removed from foemorelesszen_form_webform_client_form_alter())

function _foemorelesszen_prefill_redirect($form, &$form_state) {
  if (!$form_state['webform_completed'] || !$form_state['redirect']) {
    return;
  }

  $urn_hash = $form_state['user'];
  $nid = $form['#node']->nid;

  // Append prefill queries if we're still in session
  if (!empty($urn_hash) && $form_state['input']['prefill_session'] === 'true') {
    $redirect = $form_state['redirect'];
    $redirect[1]['query']['prefill'] = base64_encode($nid . '|' . $urn_hash); // Append urn hash
    $form_state['redirect'] = $redirect;
  }
}

