<?php


function foemorelesszen_preprocess_html (&$variables) {
  _foemorelesszen_favicon_meta();

  // Add external FA Fonts
  drupal_add_css('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array('type' => 'external', 'group' => CSS_SYSTEM, 'every_page' => TRUE));
  // Add external Google Font CSS
  drupal_add_css('https://fonts.googleapis.com/css?family=Libre+Baskerville:400,400i|Libre+Franklin:400,400i,700,700i', array('type' => 'external', 'group' => CSS_SYSTEM, 'every_page' => TRUE));

  $web_core_version = '1.10.1';

  // Grab our specified web-core from Unpkg CDN and preprocess it during aggregation
  drupal_add_css('https://unpkg.com/@foeewni/web-core@' . $web_core_version . '/dist/css/foe.core.min.css', array('type' => 'external', 'group' => CSS_SYSTEM, 'every_page' => TRUE));
  drupal_add_css('https://unpkg.com/@foeewni/web-core@' . $web_core_version . '/dist/css/foe.components.min.css', array('type' => 'external', 'group' => CSS_SYSTEM, 'every_page' => TRUE));
  drupal_add_css('https://unpkg.com/@foeewni/web-core@' . $web_core_version . '/dist/css/foe.bootstrap.min.css', array('type' => 'external', 'group' => CSS_SYSTEM, 'every_page' => TRUE));

  drupal_add_js('https://unpkg.com/@foeewni/web-core@' . $web_core_version . '/dist/js/foe.core.min.js', 'external');
  drupal_add_js('https://unpkg.com/@foeewni/web-core@' . $web_core_version . '/dist/js/foe.components.min.js', 'external');
  drupal_add_js('https://unpkg.com/@foeewni/web-core@' . $web_core_version . '/dist/js/foe.bootstrap.min.js', 'external');

  if (arg(0)=='node' && is_numeric(arg(1))) {

    $node = node_load(arg(1));

    // ---- Set up page topic and campaign variables ---- //
    //   (To line up with external datasets, e.g. Care)
    //    Ew. So fallback, much conditional, concern.
    $suppress_campaign_class = FALSE;
    $page_topic = 'unknown';
    $page_campaign = 'unknown';
    $page_campaign_class = NULL;

    //Override default Node title handling to allow for public/admin title fields
    //head_title will have already been set in theme.inc
    $public_title = field_get_items('node', $node, 'field_public_title');

    if ($public_title[0]['value']) {
      $head_title = array(
        'title' => strip_tags($public_title[0]['value']),
        'name' => check_plain(variable_get('site_name', 'Drupal')),
      );
      $variables['head_title_array'] = $head_title;
      $variables['head_title'] = implode(' | ', $head_title);
    }


    // Get the related topic node and extract values
    if ($node->type == 'topic') {
      $topic_node = $node;
    } else {
      $topic_node_ref = field_get_items('node', $node, 'field_reference_to_topic');
      if ($topic_node_ref) {
        $topic_node = node_load($topic_node_ref[0]['nid']);
      } else {
        $topic_node = NULL;
      }
    }

    if ($topic_node) {
      $page_topic_field = field_get_items('node', $topic_node, 'field_care_topic');
      if ($page_topic_field) {
        $page_topic = $page_topic_field[0]['value'];
      }
    }

    // Strip out any leading "## " and clean up for use
    $page_topic = drupal_clean_css_identifier(trim(strtolower(preg_replace('#^\\d* #', '', $page_topic))));


    // Get the related campaign node and extract values
    if ($node->type == 'campaign') {
      $campaign_node = $node;
    } elseif ($topic_node) {
      $campaign_node = node_load(field_get_items('node', $topic_node, 'field_reference_to_campaign')[0]['nid']);
    } else {
      // Legacy fallback
      $campaign_node_ref = field_get_items('node', $node, 'field_reference_to_campaign');
      if ($campaign_node_ref) {
        $campaign_node = node_load($campaign_node_ref[0]['nid']);
      } else {
        $campaign_node = NULL;
      }
    }

    if ($campaign_node) {

      $campaign_node_care_campaign = field_get_items('node', $campaign_node, 'field_care_campaign');
      if ($campaign_node_care_campaign) {
        $page_campaign = $campaign_node_care_campaign[0]['value'];
      } else {
        // Fall back to calculating a value from the node title
        $page_campaign = $campaign_node->title;
      }

      // Strip out any leading "## " and clean up for use
      $page_campaign = drupal_clean_css_identifier(trim(strtolower(preg_replace('#^\\d* #', '', $page_campaign))));

      $campaign_node_palette_name = field_get_items('node', $campaign_node, 'field_palette_name');
      if ($campaign_node_palette_name) {
        $page_campaign_class =  drupal_clean_css_identifier(strtolower($campaign_node_palette_name[0]['value']));
      } else {
        // Fall back to using the page campaign
        $page_campaign_class = $page_campaign;
      }

    }



    // Add a node-type template suggestion
    // e.g. if the node type is "donation" the template suggestion will be "html--donation.tpl.php".
    $variables['theme_hook_suggestions'][] = 'html__'. $node->type;




    // Add custom "foe:xx-yy-zz" <meta> tags for GA tracking (to be picked up by GTM)
    // Including...

    // ...The page's topic
    $meta_topic = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'foe:topic',
        'content' => $page_topic
      ],
    ];
    drupal_add_html_head($meta_topic, 'foe:topic');

    // ...The page's campaign
    $meta_campaign = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'foe:campaign',
        'content' => $page_campaign
      ],
    ];
    drupal_add_html_head($meta_campaign, 'foe:campaign');

    // ...The page's "content area"
    // (Legacy, can be removed once GTM is switched to report on foe:campaign and foe:topic instead)
    $meta_primary_content_area = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'foe:primary-content-area',
        'content' => $page_campaign
      ],
    ];
    drupal_add_html_head($meta_primary_content_area, 'foe:primary-content-area');

    // ...The type of node, if we're in a full node view, or "other"
    $page_type = $node->type ?: 'other';

    switch ($page_type) {
      // (Translates "webform" to the more accurate "flexible_form")
      // (Translates "static_page" to the more descriptive "campaignion_page")
      case 'webform' :
        $page_type = 'flexible_form';
        break;
      case 'static_page' :
        $page_type = 'campaignion_page';
        break;
    }

    $meta_page_type = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'foe:page-type',
        'content' => $page_type,
      ],
    ];
    drupal_add_html_head($meta_page_type, 'foe:page-type');


    // ...Whether the page has a hero image (From D8). Redundant here as we don't have
    // hero banners, but perhaps this could be re-purposed as a "layout option" value?
    $meta_has_hero = [
      '#tag' => 'meta',
      '#attributes' => [
        'name' => 'foe:has-hero',
        'content' => 'no',
      ],
    ];
    drupal_add_html_head($meta_has_hero, 'foe:has-hero');

    $params = drupal_get_query_parameters();

    // Add a HTML <body> element class to indicate a layout switch
    $variables['classes_array'][] = 'layout-' . _foemorelesszen_get_layout();

    if ($node->type === 'thank_you_page') {
      $scroll_field = field_get_items('node', $node, 'field_scrolling_active');

      if (isset($params['quiz']) && $params['quiz'] > 0) {
        // Quiz query parameter takes precedence
        $suppress_campaign_class = true;
        $variables['classes_array'][] = 'node-type-thank-you-quiz';
      } elseif (!empty($scroll_field) && isset($scroll_field) && $scroll_field[0]['value'] === '1') {
        // If not quiz then check if it's a scrolling one
        $variables['classes_array'][] = 'node-type-thank-you-scroll';
      } else {
        // Fallback to default, we don't need it but heh
        $variables['classes_array'][] = 'node-type-thank-you-default';
      }
    }

    if ($node->type === 'quiz') {
      $suppress_campaign_class = true;
    }


    // Use special preview-and-redirect share template for custom shares
    if ($node->type == 'quiz'
      && isset($params['share_data'])
      && NULL != ($share_data = json_decode(base64_decode($params['share_data'])))
      ) {

        // Add the template suggestion
      $variables['theme_hook_suggestions'][] = 'html__quiz_share';

      // Set up values and variables
      $share_image = $node->opengraph_meta_image['und'][0];

      $topic_node = node_load(
        $node->quiz_linked_form['und'][0]['entity']
        ->field_reference_to_topic['und'][0]['nid']
      );

      $campaign_node = node_load(
        $topic_node->field_reference_to_campaign['und'][0]['nid']
      );

      $quiz_config = json_decode($node->quiz_config['und'][0]['value']);
      if ($quiz_config == null) {
        $quiz_config = [];
      }

      $quiz_score = htmlspecialchars($share_data->score);
      $quiz_question_count = count($quiz_config);


      // Default share copy
      $share_title = 'I got !score out of !questions right. Can you do better?';
      $share_description = $node->title;

      // Custom share copy, if available
      if (isset($node->field_custom_share_title) && !empty($node->field_custom_share_title)) {
        $share_title = $node->field_custom_share_title['und'][0]['safe_value'];
      }
      if (isset($node->field_custom_share_description) && !empty($node->field_custom_share_description)) {
        $share_description = $node->field_custom_share_description['und'][0]['safe_value'];
      }

      // Replace tokens with values
      // TODO: Make/use a helper function?
      $share_title = str_replace(
        ['!score', '!questions'],
        [$quiz_score, $quiz_question_count],
        $share_title
      );
      $share_description = str_replace(
        ['!score', '!questions'],
        [$quiz_score, $quiz_question_count],
        $share_description
      );


      // Collate all info needed to generate the custom share HTML
      // Yucky.
      $variables['share_data'] = [

        'raw' => $params['share_data'],
        'value' => htmlspecialchars($params['share_data']),

        'title' => $share_title,
        'description' => $share_description,

        'topic' => $topic_node->field_care_topic['und'][0]['safe_value'],
        'campaign' => $campaign_node->field_care_campaign['und'][0]['safe_value'],

        'target_title' => $node->title,
        'target_url' => url(drupal_get_path_alias(), ['absolute' => TRUE]),

        'image_url' => file_create_url($share_image['uri']),
        'image_height' => $share_image['height'],
        'image_width' => $share_image['width'],

      ];

    }

    // Add a HTML <body> element class referencing the $page_campaign value (used for pallete switching)
    if ($page_campaign_class && !$suppress_campaign_class) {
      $variables['classes_array'][] = 'campaign-' . $page_campaign_class;
    }

  }
}
