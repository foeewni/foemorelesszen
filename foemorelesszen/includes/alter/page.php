<?php

use \Drupal\little_helpers\Webform\Submission;

/**
 * Populate details from a previous submission so they're easily available to Google Tag Manager.
 * Values exposed via Drupal.settings.foemorelesszen.submission_details
 * Nicked and modified from the campaignion_google_analytics module.
 */

function foemorelesszen_page_alter (&$page) {

  if (!isset($page['content']['metatags']['node:thank_you_page'])) {
    // Bail out if we're not on a thank you page
    return;
  }

  $parameters = drupal_get_query_parameters();

  if (!isset($parameters['sid'])) {
    // Bail out if we don't have a submission ID
    return;
  }

  $sid = intval($parameters['sid']);
  $nid = db_query('SELECT nid FROM {webform_submissions} WHERE sid=:sid', [':sid' => $sid])->fetchField();
  if (!($submission = Submission::load($nid, $sid))) {
    return;
  }
  $node = $submission->getNode();

  // General information about the submission node
  drupal_add_js(
    array(
      'foemorelesszen' => array(
        'submission_details' => array(
          'nid' => $node->nid,
          'language' => $node->language,
          'title' => $node->title,
          'actions' => [],
        ),
      ),
    ),
    'setting');

    if (!empty($submission->payments) && ($payment = reset($submission->payments))) {
    $donation_title = $node->title;
    $donation_lang = $node->language;
    $interval = '';
    switch ($submission->valueByKey('donation_interval')) {
    case '1':
    case NULL: // If donation_interval isn't set the payment method selector provides the cash payment options
      $interval = 'only once';
      break;
    case 'm':
      $interval = 'monthly';
      break;
    case 'y':
      $interval = 'yearly';
      break;
    default:
      $interval = "unknown [" . $submission->valueByKey('donation_interval') . "]";
    }
    $product_name = $donation_title . ' [' . $donation_lang . ']';
    $amount = $payment->totalAmount(false);

    // TODO: Trim this down to provide only what we actually need
    // (So we can just reference it in GTM directly.)
    drupal_add_js(
      array(
        'foemorelesszen' => array(
          'submission_details' => array(
            'actions' => ['purchase'],
            'product' => array(
              'id' => $node->nid,
              'name' => $product_name,
              'price' => $amount,
              'category' => $interval,
              'currency' => $payment->currency_code,
              'quantity' => '1',
            ),
            'purchase' => array(
              'id' => $payment->pid,
              'revenue' => $amount,
              'currency' => $payment->currency_code,
        )))),
      'setting');
  }

}
